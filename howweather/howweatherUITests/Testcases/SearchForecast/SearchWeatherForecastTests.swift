//
//  SearchWeatherForecastTests.swift
//  howweatherUITests
//
//  Created by Kieu Anh Huy on 24/5/21.
//

import XCTest

class SearchWeatherForecastTests: XCTestCase {
    var app: XCUIApplication!

    private enum Scenario: String {
        case validSearchText = "Mel"
        case notFoundSearchText = "NotFound"
        case networkTimeout = "networkTimeout"
        case noNetworkConnection = "noNetworkConnection"
        case unknownError = "unknownError"
    }

    override func setUpWithError() throws {
        continueAfterFailure = false
        app = XCUIApplication()
        app.launch()
    }

    override func tearDownWithError() throws {
        app.terminate()
    }

    func test_ClearSearchText_ShouldClearForecastListAndShowInputRequireMessage() {
        let searchScreen = SearchWeatherForecastScreen(app: app)
        _ = searchScreen
            .typeSearchText(Scenario.validSearchText.rawValue)
            .clearSearchText()
            .verifyMessage("Please type your city")
    }

    func test_CancelSearch_ClearForecastListAndShowInputRequireMessage() {
        let searchScreen = SearchWeatherForecastScreen(app: app)
        _ = searchScreen
            .typeSearchText(Scenario.validSearchText.rawValue)
            .cancelSearchText()
            .verifyMessage("Please type your city")
    }

    func test_Search_WithInvalidLength_ShouldNotSearch() {
        let searchScreen = SearchWeatherForecastScreen(app: app)
        _ = searchScreen
            .typeSearchText("12")
            .verifyMessage("Please type your city")
    }

    func test_Search_WithValidText_ShouldShowForecast() {
        let searchScreen = SearchWeatherForecastScreen(app: app)

        let expectedFirstForecastData = ExpectedForecastData(date: "Date: Mon, 24 May 2021",
                                                             pressure: "Pressure: 1016",
                                                             humidity: "Humidity: 98%",
                                                             weatherDescription: "Description: heavy intensity rain",
                                                             temperature: "Average Temperature: 13°C")

        _ = searchScreen
            .typeSearchText(Scenario.validSearchText.rawValue)
            .verifyShowedForecastWithExpectedData(expectedFirstForecastData)
    }

    func test_Search_WithWrongText_ShouldShowForecast() {
        let searchScreen = SearchWeatherForecastScreen(app: app)
        _ = searchScreen
            .typeSearchText(Scenario.notFoundSearchText.rawValue)
            .verifyMessage("No Forecast Found")
    }

    func test_Search_WithNoInternetConnection_ShouldShowErrorMessage() {
        let searchScreen = SearchWeatherForecastScreen(app: app)
        _ = searchScreen
            .typeSearchText(Scenario.noNetworkConnection.rawValue)
            .verifyMessage("Please check your network connection")
    }

    func test_Search_WithConnectionTimeout_ShouldShowErrorMessage() {
        let searchScreen = SearchWeatherForecastScreen(app: app)
        _ = searchScreen
            .typeSearchText(Scenario.networkTimeout.rawValue)
            .verifyMessage("Please check your network connection")
    }

    func test_Search_WithUnknowError_ShouldShowErrorMessage() {
        let searchScreen = SearchWeatherForecastScreen(app: app)
        _ = searchScreen
            .typeSearchText(Scenario.unknownError.rawValue)
            .verifyMessage("We're unable to process your request. Please try again later")
    }
}
