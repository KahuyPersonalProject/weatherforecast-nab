//
//  Screen.swift
//  howweatherUITests
//
//  Created by Kieu Anh Huy on 24/5/21.
//

import XCTest
@testable import howweather

class SearchWeatherForecastScreen {
    let app: XCUIApplication
    private enum ElementIdentifiers {
        static let tableView = "accessibilityId.searchWeatherForecast.tableView"
        static let searchbar = "accessibilityId.searchWeatherForecast.searchBar"
        static let messageLabel = "accessibilityId.searchWeatherForecast.messageLabel"
        static let forecastDateLabel = "accessibilityId.searchWeatherForecast.forecastDateLabel"
        static let forecastHumidityLabel = "accessibilityId.searchWeatherForecast.forecastHumidityLabel"
        static let forecastPressureLabel = "accessibilityId.searchWeatherForecast.forecastPressureLabel"
        static let forecastTemperatureLabel = "accessibilityId.searchWeatherForecast.forecastTemperatureLabel"
        static let forecastDescrptionLabel = "accessibilityId.searchWeatherForecast.forecastDescrptionLabel"
    }

    init(app: XCUIApplication) {
        self.app = app
    }

    func typeSearchText(_ text: String) -> Self {
        let searchField = app
            .otherElements[ElementIdentifiers.searchbar]
            .searchFields
            .firstMatch
        searchField.tap()
        searchField.typeText(text)
        return self
    }

    func clearSearchText() -> Self {
        let searchField = app
            .otherElements[ElementIdentifiers.searchbar]
            .searchFields
            .firstMatch
        searchField.buttons["Clear text"].tap()
        return self
    }

    func cancelSearchText() -> Self {
        let cancelButton = app
            .otherElements[ElementIdentifiers.searchbar]
            .buttons
            .firstMatch
        cancelButton.tap()
        return self
    }

    func verifyMessage(_ message: String) -> Self {
        let messageLabel = getLabelById(ElementIdentifiers.messageLabel, from: app)
        XCTAssertTrue(messageLabel.waitForExistence(timeout: 5))
        XCTAssertEqual(messageLabel.label, message)
        return self
    }

    func verifyShowedForecastWithExpectedData(_ data: ExpectedForecastData) -> Self {
        let firstCell = app.tables.element(boundBy: 0).cells.element(boundBy: 0)
        XCTAssertTrue(firstCell.waitForExistence(timeout: 5))
        let dateLabel = getLabelById(ElementIdentifiers.forecastDateLabel, from: firstCell)
        let humidityLabel = getLabelById(ElementIdentifiers.forecastHumidityLabel, from: firstCell)
        let pressureLabel = getLabelById(ElementIdentifiers.forecastPressureLabel, from: firstCell)
        let temperatureLabel = getLabelById(ElementIdentifiers.forecastTemperatureLabel, from: firstCell)
        let descritionLabel = getLabelById(ElementIdentifiers.forecastDescrptionLabel, from: firstCell)
        XCTAssertTrue(dateLabel.exists)
        XCTAssertEqual(dateLabel.label, data.date)

        XCTAssertTrue(humidityLabel.exists)
        XCTAssertEqual(humidityLabel.label, data.humidity)

        XCTAssertTrue(pressureLabel.exists)
        XCTAssertEqual(pressureLabel.label, data.pressure)

        XCTAssertTrue(temperatureLabel.exists)
        XCTAssertEqual(temperatureLabel.label, data.temperature)

        XCTAssertTrue(descritionLabel.exists)
        XCTAssertEqual(descritionLabel.label, data.weatherDescription)
        return self
    }

    private func getLabelById(_ lalbeId: String,
                              from element: XCUIElement) -> XCUIElement {
        return element.staticTexts
            .element(matching: .any,
                     identifier: lalbeId)
    }
}

struct ExpectedForecastData {
    let date: String
    let pressure: String
    let humidity: String
    let weatherDescription: String
    let temperature: String
}
