//
//  NetworkClientTests.swift
//  howweatherTests
//
//  Created by Kieu Anh Huy on 24/5/21.
//

import XCTest
@testable import howweather

class NetworkClientTests: XCTestCase {
    var networkClient: NetworkClient!
    var urlSession: URLSession!

    override func setUpWithError() throws {
        let configuration = URLSessionConfiguration.default
        configuration.protocolClasses = [NetworkProtocolMock.self]
        urlSession = URLSession(configuration: configuration)
        networkClient = NetworkClient(baseUrl: AppConstant.Common.baseForecastUrl,
                                      session: urlSession)
    }

    override func tearDownWithError() throws {
        urlSession = nil
        networkClient = nil
        NetworkProtocolMock.loadingHandler = nil
    }

    func test_Request_WithInvalidPath_ShouldReturnInvalidError() {
        // When
        var response: Result<Data, Error>?
        networkClient.request(path: "*&%&&^&*^\\///*Y(*(*HIH",
                              method: .get,
                              parameterEncoder: ParameterEncoder.queryString,
                              requestParameter: nil,
                              completion: { result in
                                response = result
                              })

        // Then
        if case .failure(let error) = response,
           case NetworkClientError.invalidUrl = error {
            XCTAssertTrue(true)
        } else {
            XCTAssertTrue(false)
        }
    }

    func test_Request_WithSuccessResponseAndValidData_ShouldReturnSuccess() {
        // Given
        let value = "textValue"
        let rawData = "{\"text\": \"\(value)\"}"
        let testData = rawData.data(using: .utf8)!
        NetworkProtocolMock.loadingHandler = { request -> (HTTPURLResponse, Data?, Error?) in
            let response = HTTPURLResponse(url: request.url!,
                                           statusCode: 200,
                                           httpVersion: nil,
                                           headerFields: nil)!
            return (response, testData, nil)
        }

        // When
        let callbackExpectation = expectation(description: "WithSuccessResponseAndValidData")
        var response: Result<Data, Error>?
        networkClient.request(path: "/Test",
                              method: .get,
                              parameterEncoder: ParameterEncoder.queryString,
                              requestParameter: ["key": "value"],
                              completion: { result in
                                response = result
                                callbackExpectation.fulfill()
                              })
        wait(for: [callbackExpectation], timeout: 2)

        // Then
        if case .success(let data) = response,
           data == testData {
            XCTAssertTrue(true)
        } else {
            XCTAssertTrue(false)
        }
    }

    func test_Request_WithSuccessResponseAndInValidData_ShouldReturnFailure() {
        // Given!
        NetworkProtocolMock.loadingHandler = { request -> (HTTPURLResponse, Data?, Error?) in
            let response = HTTPURLResponse(url: request.url!,
                                           statusCode: 200,
                                           httpVersion: nil,
                                           headerFields: nil)!
            return (response, Data(), nil)
        }

        // When
        let callbackExpectation = expectation(description: "WithSuccessResponseAndInValidData")
        var response: Result<Data, Error>?
        networkClient.request(path: "/Test",
                              method: .get,
                              parameterEncoder: ParameterEncoder.queryString,
                              requestParameter: ["key": "value"],
                              completion: { result in
                                response = result
                                callbackExpectation.fulfill()
                              })
        wait(for: [callbackExpectation], timeout: 2)

        // Then
        if case .failure(let error) = response,
           case NetworkClientError.unknown = error {
            XCTAssertTrue(true)
        } else {
            XCTAssertTrue(false)
        }
    }

    func test_Request_WithHttpErrorResponse_ShouldReturnFailure() {
        // Given!
        NetworkProtocolMock.loadingHandler = { request -> (HTTPURLResponse, Data?, Error?) in
            let response = HTTPURLResponse(url: request.url!,
                                           statusCode: 404,
                                           httpVersion: nil,
                                           headerFields: nil)!
            let value = "textValue"
            let rawData = "{\"text\": \"\(value)\"}"
            let testData = rawData.data(using: .utf8)!
            return (response, testData, nil)
        }

        // When
        let callbackExpectation = expectation(description: "WithHttpErrorResponse")
        var response: Result<Data, Error>?
        networkClient.request(path: "/Test",
                              method: .get,
                              parameterEncoder: ParameterEncoder.queryString,
                              requestParameter: ["key": "value"],
                              completion: { result in
                                response = result
                                callbackExpectation.fulfill()
                              })
        wait(for: [callbackExpectation], timeout: 2)

        // Then
        if case .failure(let error) = response,
           case NetworkClientError.httpError(let statusCode, _, let data) = error {
            XCTAssertEqual(statusCode, 404)
            XCTAssertNotNil(data)
        } else {
            XCTAssertTrue(false)
        }
    }

    func test_Request_WithInternetErrorResponse_ShouldReturnFailure() {
        // Given!
        NetworkProtocolMock.loadingHandler = { request -> (HTTPURLResponse, Data?, Error?) in
            let response = HTTPURLResponse(url: request.url!,
                                           statusCode: 404,
                                           httpVersion: nil,
                                           headerFields: nil)!
            let error = NSError(domain: "",
                                code: URLError.notConnectedToInternet.rawValue,
                                userInfo: nil)
            return (response, nil, error)
        }

        // When
        let callbackExpectation = expectation(description: "WithInternetErrorResponse")
        var response: Result<Data, Error>?
        networkClient.request(path: "/Test",
                              method: .get,
                              parameterEncoder: ParameterEncoder.queryString,
                              requestParameter: ["key": "value"],
                              completion: { result in
                                response = result
                                callbackExpectation.fulfill()
                              })
        wait(for: [callbackExpectation], timeout: 2)

        // Then
        if case .failure(let error) = response,
           case NetworkClientError.noNetworkConnection = error {
            XCTAssertTrue(true)
        } else {
            XCTAssertTrue(false)
        }
    }

    func test_Request_WithTimeOutErrorResponse_ShouldReturnFailure() {
        // Given!
        NetworkProtocolMock.loadingHandler = { request -> (HTTPURLResponse, Data?, Error?) in
            let response = HTTPURLResponse(url: request.url!,
                                           statusCode: 404,
                                           httpVersion: nil,
                                           headerFields: nil)!
            let error = NSError(domain: "",
                                code: URLError.timedOut.rawValue,
                                userInfo: nil)
            return (response, nil, error)
        }

        // When
        let callbackExpectation = expectation(description: "WithTimeOutErrorResponse")
        var response: Result<Data, Error>?
        networkClient.request(path: "/Test",
                              method: .get,
                              parameterEncoder: ParameterEncoder.queryString,
                              requestParameter: ["key": "value"],
                              completion: { result in
                                response = result
                                callbackExpectation.fulfill()
                              })
        wait(for: [callbackExpectation], timeout: 2)

        //Then
        if case .failure(let error) = response,
           case NetworkClientError.timeout = error {
            XCTAssertTrue(true)
        } else {
            XCTAssertTrue(false)
        }
    }

    func test_Request_WithUnknownErrorResponse_ShouldReturnFailure() {
        // Given!
        NetworkProtocolMock.loadingHandler = { request -> (HTTPURLResponse, Data?, Error?) in
            let response = HTTPURLResponse(url: request.url!,
                                           statusCode: 404,
                                           httpVersion: nil,
                                           headerFields: nil)!
            let error = NSError(domain: "",
                                code: URLError.unknown.rawValue,
                                userInfo: nil)
            return (response, nil, error)
        }

        // When
        let callbackExpectation = expectation(description: "WithUnknownErrorResponse")
        var response: Result<Data, Error>?
        networkClient.request(path: "/Test",
                              method: .get,
                              parameterEncoder: ParameterEncoder.queryString,
                              requestParameter: ["key": "value"],
                              completion: { result in
                                response = result
                                callbackExpectation.fulfill()
                              })
        wait(for: [callbackExpectation], timeout: 2)

        // Then
        if case .failure(let error) = response,
           case NetworkClientError.unknown = error {
            XCTAssertTrue(true)
        } else {
            XCTAssertTrue(false)
        }
    }
}

class NetworkProtocolMock: URLProtocol {
    static var loadingHandler: ((URLRequest) -> (HTTPURLResponse, Data?, Error?))?

    override class func canInit(with request: URLRequest) -> Bool {
        return true
    }

    override class func canonicalRequest(for request: URLRequest) -> URLRequest {
        return request
    }

    override class func requestIsCacheEquivalent(_ lhs: URLRequest, to rhs: URLRequest) -> Bool {
        return false
    }

    override func startLoading() {
        guard let handler = NetworkProtocolMock.loadingHandler else {
            XCTFail("Loading handler is not set.")
            return
        }
        let (response, data, error) = handler(request)
        if let data = data {
            client?.urlProtocol(self, didReceive: response, cacheStoragePolicy: .notAllowed)
            client?.urlProtocol(self, didLoad: data)
            client?.urlProtocolDidFinishLoading(self)
        } else {
            client?.urlProtocol(self, didFailWithError: error!)
        }
    }

    override func stopLoading() {
    }
}
