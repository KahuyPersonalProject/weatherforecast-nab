//
//  SimpleDependencyContainerTests.swift
//  howweatherTests
//
//  Created by Kieu Anh Huy on 24/5/21.
//

import XCTest
@testable import howweather

class SimpleDependencyContainerTests: XCTestCase {
    var container: SimpleDependencyContainer!
    override func setUpWithError() throws {
        container = SimpleDependencyContainer()
    }

    override func tearDownWithError() throws {
        container = nil
    }

    func test_RegisterWithSingletonScope_ShouldReturnSameInstance() {
        container.register(DependencyContainerLoadingIndicator.self,
                           name: nil,
                           factory: { _ in DependencyContainerLoadingIndicatorImpl() },
                           scope: .container)
        let firstLoadingIndicator = container.resolve(DependencyContainerLoadingIndicator.self)
        let secondLoadingIndicator = container.resolve(DependencyContainerLoadingIndicator.self)
        let isSameInstance = firstLoadingIndicator === secondLoadingIndicator
        XCTAssertTrue(isSameInstance, "2 instance is point to the same object")
    }

    func test_RegisterWithDefaultNameAndSingletonScope_ShouldReturnSameInstance() {
        container.register(DependencyContainerLoadingIndicator.self,
                           factory: { _ in DependencyContainerLoadingIndicatorImpl() },
                           scope: .container)
        let firstLoadingIndicator = container.resolve(DependencyContainerLoadingIndicator.self)
        let secondLoadingIndicator = container.resolve(DependencyContainerLoadingIndicator.self)
        let isSameInstance = firstLoadingIndicator === secondLoadingIndicator
        XCTAssertTrue(isSameInstance, "2 instance is point to the same object")
    }

    func test_RegisterWithSingletonScopeAndDifferenceName_ShouldReturnDifferenceInstance() {
        let name1 = "DependencyContainerLoadingIndicator1"
        let name2 = "DependencyContainerLoadingIndicator2"
        container.register(DependencyContainerLoadingIndicator.self,
                           name: name1,
                           factory: {_  in DependencyContainerLoadingIndicatorImpl() },
                           scope: .container)
        container.register(DependencyContainerLoadingIndicator.self,
                           name: name2,
                           factory: { _ in DependencyContainerLoadingIndicatorImpl() },
                           scope: .container)
        let firstLoadingIndicator = container.resolve(DependencyContainerLoadingIndicator.self, name: name1)
        let secondLoadingIndicator = container.resolve(DependencyContainerLoadingIndicator.self, name: name2)
        let isSameInstance = firstLoadingIndicator !== secondLoadingIndicator
        XCTAssertTrue(isSameInstance, "2 instance is point to the same object")
    }

    func test_RegisterWithTransientScope_ShouldReturnDifferentInstance() {
        container.register(DependencyContainerLoadingIndicator.self,
                           factory: { _ in DependencyContainerLoadingIndicatorImpl() },
                           scope: .transient)
        let firstLoadingIndicator = container.resolve(DependencyContainerLoadingIndicator.self)
        let secondLoadingIndicator = container.resolve(DependencyContainerLoadingIndicator.self)
        let isSameInstance = firstLoadingIndicator !== secondLoadingIndicator
        XCTAssertTrue(isSameInstance, "2 instance is not point to the same object")
    }

    func test_ResolveWithDefaultName_ShouldReturnInstance() {
        container.register(DependencyContainerLoadingIndicator.self,
                           factory: { _ in DependencyContainerLoadingIndicatorImpl() },
                           scope: .transient)
        let firstLoadingIndicator = container.resolve(DependencyContainerLoadingIndicator.self)
        XCTAssertNotNil(firstLoadingIndicator)
    }
}

protocol DependencyContainerLoadingIndicator: AnyObject {
    func show()
}

class DependencyContainerLoadingIndicatorImpl: DependencyContainerLoadingIndicator {
    func show() {
    }
}
