//
//  DispatchAsyncTests.swift
//  howweatherTests
//
//  Created by Kieu Anh Huy on 24/5/21.
//

import XCTest
@testable import howweather

class DispatchAsyncTests: XCTestCase {

    func test_DispatchMain_ShouldCallInMainThread() {
        // When
        let expection = expectation(description: "test Dispatch Main Should Call Main Thread")
        var isMainThread = false
        DispatchAsync.main.dispatch {
            isMainThread = Thread.isMainThread
            expection.fulfill()
        }
        wait(for: [expection], timeout: 1)

        // Then
        XCTAssertTrue(isMainThread)
    }

    func test_DispatchGlobal_ShouldCallNotMainThread() {
        // When
        let expection = expectation(description: "test Dispatch Global Should Call Not call Main Thread")
        var isMainThread = true
        DispatchAsync.global.dispatch {
            isMainThread = Thread.isMainThread
            expection.fulfill()
        }

        wait(for: [expection], timeout: 1)

        // Then
        XCTAssertFalse(isMainThread)
    }
}
