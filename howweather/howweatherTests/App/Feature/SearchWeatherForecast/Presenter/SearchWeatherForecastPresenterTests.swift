//
//  SearchWeatherForecastPresenterTests.swift
//  howweatherTests
//
//  Created by Kieu Anh Huy on 23/5/21.
//

import XCTest
@testable import howweather

class SearchWeatherForecastPresenterTests: XCTestCase {

    var presenter: SearchWeatherForecastPresenter!
    var localisationStub: LocalizableStub!
    var dispatchQueues: DispatchQueues!
    var interactor: SearchWeatherForecastInteractorMock!
    var view: SearchWeatherForecastViewableMock!

    override func setUpWithError() throws {
        view = SearchWeatherForecastViewableMock()
        localisationStub = LocalizableStub()
        dispatchQueues = DispatchQueueImpl(mainDispatch: DispatchSyncStub.global,
                                           backgroundDispatch: DispatchSyncStub.background)
        interactor = SearchWeatherForecastInteractorMock()
        presenter = SearchWeatherForecastPresenter(interactor: interactor,
                                                   dispatchQueues: dispatchQueues,
                                                   localisation: localisationStub)
    }

    override func tearDownWithError() throws {
        presenter = nil
        dispatchQueues = nil
        localisationStub = nil
        interactor = nil
    }

    func test_Attach_ShouldSetViewAndUpdateScreenTitle() {
        // When
        presenter.attach(view: view)

        // Then
        XCTAssertEqual(view.numberShowTitleCall, 1)
        XCTAssertEqual(view.updatedTitle, LocalisationKey.SearchWeatherForecast.title)
    }

    func test_CancelSearch_ShouldClearCurrentSearchTextAndShowRequireInputMesssage() {
        // Given
        presenter.currentSearchText = "123"
        presenter.attach(view: view)

        // When
        presenter.cancelSearch()

        // Then
        XCTAssertNil(presenter.currentSearchText)
        XCTAssertEqual(view.numberReloadCellCalled, 1)
        if let messageCell = view.cells.first,
           case .message(let message) = messageCell {
            XCTAssertEqual(message, LocalisationKey.SearchWeatherForecast.inputSearch)
        } else {
            XCTAssertTrue(false)
        }
    }

    func test_SearchText_WithInvalidTextLength_ShouldShowRequireMessageAndNotCallSearch() {
        // Given
        presenter.attach(view: view)
        let searchText = "te"

        // When
        presenter.search(searchText)

        // Then
        XCTAssertEqual(presenter.currentSearchText, searchText)
        XCTAssertEqual(view.numberReloadCellCalled, 1)
        XCTAssertEqual(interactor.numberSearchCalled, 0)
        if let messageCell = view.cells.first,
           case .message(let message) = messageCell {
            XCTAssertEqual(message, LocalisationKey.SearchWeatherForecast.inputSearch)
        } else {
            XCTAssertTrue(false)
        }
    }

    func test_SearchText_WithValidTextLenth_ShouldShowLoading() {
        // Given
        presenter.attach(view: view)
        let searchText = "tex"

        // When
        presenter.search(searchText)

        // Then
        XCTAssertEqual(presenter.currentSearchText, searchText)
        XCTAssertEqual(view.numberReloadCellCalled, 1)
        if let loadingCell = view.cells.first,
           case .loading = loadingCell {
            XCTAssertTrue(true)
        } else {
            XCTAssertTrue(false)
        }
    }

    func test_SearchText_WithValidTextLenth_ShouldCallApi() {
        // Given
        presenter.attach(view: view)
        let searchText = "tex"

        // When
        presenter.search(searchText)

        // Then
        XCTAssertEqual(interactor.searchTextCalled, searchText)
        XCTAssertEqual(interactor.numberSearchCalled, 1)
    }

    func test_SearchText_WithTimeOutError_ShouldShowErrorMessage() {
        // Given
        interactor.expectResult = .failure(AppError.timeout)
        presenter.attach(view: view)
        let searchText = "tex"

        // When
        presenter.search(searchText)

        // Then
        if let messageCell = view.cells.first,
           case .message(let message) = messageCell {
            XCTAssertEqual(message, LocalisationKey.Common.networkConnectionError)
        } else {
            XCTAssertTrue(false)
        }
    }

    func test_SearchText_WithNoNetworkConnectionError_ShouldShowErrorMessage() {
        // Given
        interactor.expectResult = .failure(AppError.noNetworkConnection)
        presenter.attach(view: view)
        let searchText = "tex"

        // When
        presenter.search(searchText)

        // Then
        if let messageCell = view.cells.first,
           case .message(let message) = messageCell {
            XCTAssertEqual(message, LocalisationKey.Common.networkConnectionError)
        } else {
            XCTAssertTrue(false)
        }
    }

    func test_SearchText_WithForecastNotFound_ShouldShowErrorMessage() {
        // Given
        interactor.expectResult = .failure(WeatherForecastError.forecastNotFound)
        presenter.attach(view: view)
        let searchText = "tex"

        // When
        presenter.search(searchText)

        // Then
        if let messageCell = view.cells.first,
           case .message(let message) = messageCell {
            XCTAssertEqual(message, LocalisationKey.SearchWeatherForecast.noForecastFound)
        } else {
            XCTAssertTrue(false)
        }
    }

    func test_SearchText_WithGenericError_ShouldShowErrorMessage() {
        // Given
        interactor.expectResult = .failure(AppError.genericError(error: nil))
        presenter.attach(view: view)
        let searchText = "tex"

        // When
        presenter.search(searchText)

        // Then
        if let messageCell = view.cells.first,
           case .message(let message) = messageCell {
            XCTAssertEqual(message, LocalisationKey.Common.genericError)
        } else {
            XCTAssertTrue(false)
        }
    }

    func test_SearchText_WithSuccessResponse_ShouldForecastList() {
        // Given
        let model = WeatherForecastModel(date: Date(),
                                         pressure: 1,
                                         humidity: 1,
                                         weatherDescription: "weatherDescription",
                                         temperature: TemperatureModel(max: 1, min: 5))
        interactor.expectResult = .success([model])
        presenter.attach(view: view)
        let searchText = "tex"

        // When
        presenter.search(searchText)

        // Then
        XCTAssertEqual(view.numberReloadCellCalled, 2)
        XCTAssertEqual(view.cells.count, 1)
        if let forecastCell = view.cells.first,
           case .forecast(_) = forecastCell {
            XCTAssertTrue(true)
        } else {
            XCTAssertTrue(false)
        }
    }

    func test_SearchText_WithEmptySuccessResponse_ShouldShowErrorMessage() {
        // Given
        interactor.expectResult = .success([])
        presenter.attach(view: view)
        let searchText = "tex"

        // When
        presenter.search(searchText)

        // Then
        XCTAssertEqual(view.numberReloadCellCalled, 2)
        XCTAssertEqual(view.cells.count, 1)
        if let messageCell = view.cells.first,
           case .message(let message) = messageCell {
            XCTAssertEqual(message, LocalisationKey.SearchWeatherForecast.noForecastFound)
        } else {
            XCTAssertTrue(false)
        }
    }
}

class SearchWeatherForecastInteractorMock: SearchWeatherForecastInteractable {
    var numberSearchCalled = 0
    var searchTextCalled = ""
    var expectResult: ResultData?
    func searchWeatherForecastForTerm(_ term: String,
                                      completion: @escaping (ResultData) -> Void) {
        numberSearchCalled += 1
        searchTextCalled = term
        if let expectResult = expectResult {
            completion(expectResult)
        }
    }
}

class SearchWeatherForecastViewableMock: SearchWeatherForecastViewable {
    var numberShowTitleCall = 0
    var updatedTitle: String = ""
    var numberReloadCellCalled = 0
    var cells: [WeatherForecastCellType] = []

    func reloadCells(_ cells: [WeatherForecastCellType]) {
        numberReloadCellCalled += 1
        self.cells = cells
    }

    func showTitle(_ title: String) {
        numberShowTitleCall += 1
        updatedTitle = title
    }
}
