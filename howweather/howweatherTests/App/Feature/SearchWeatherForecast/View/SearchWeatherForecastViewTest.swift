//
//  SearchWeatherForecastViewTest.swift
//  howweatherTests
//
//  Created by Kieu Anh Huy on 23/5/21.
//

import XCTest
@testable import howweather

class SearchWeatherForecastViewTest: XCTestCase {
    var view: SearchWeatherForecastView!

    override func setUpWithError() throws {
        view = SearchWeatherForecastView()
    }

    override func tearDownWithError() throws {
        view = nil
    }

    func test_ReloadData_ShouldUpdateDataSource() {
        // Given
        let dataSourceMock = SearchWeatherForecastDataSourceMock()
        view.resultListDataSource = dataSourceMock

        // When
        view.reloadData([.loading])

        // Then
        XCTAssertEqual(dataSourceMock.numberUpdateCellCalled, 1)
        XCTAssertEqual(dataSourceMock.updatedCells.count, 1)
    }

    func test_SearchBarCancelButtonClicked_ShouldClearTextAndCallCancelDelegate() {
        // Given
        let mockDelegate = SearchWeatherForecastViewDelegateMock()
        view.delegate = mockDelegate
        let searchBar = UISearchBar()
        searchBar.text = "123"

        // When
        view.searchBarCancelButtonClicked(searchBar)

        // Then
        XCTAssertEqual(searchBar.text, "")
        XCTAssertTrue(mockDelegate.didCallCancelSearch)
    }

    func test_SearchBarTextDidChange_ShouldCallDidChangeSearchTextDelegate() {
        // Given
        let mockDelegate = SearchWeatherForecastViewDelegateMock()
        view.delegate = mockDelegate
        let searchBar = UISearchBar()
        let changedText = "123"

        // When
        view.searchBar(searchBar, textDidChange: changedText)

        // Then
        XCTAssertTrue(mockDelegate.didCallChangeSearchText)
        XCTAssertEqual(mockDelegate.searchText, changedText)
    }
}

class SearchWeatherForecastDataSourceMock: SearchWeatherForecastDataSource {
    var numberUpdateCellCalled = 0
    var updatedCells: [WeatherForecastCellType] = []
    override func updateCells(_ cells: [WeatherForecastCellType]) {
        numberUpdateCellCalled += 1
        updatedCells = cells
    }
}

class SearchWeatherForecastViewDelegateMock: SearchWeatherForecastViewDelegate {
    var didCallCancelSearch = false
    var didCallChangeSearchText = false
    var searchText: String = ""

    func didCancelSearch() {
        didCallCancelSearch = true
    }

    func didChangeSearchText(_ text: String) {
        didCallChangeSearchText = true
        searchText = text
    }
}
