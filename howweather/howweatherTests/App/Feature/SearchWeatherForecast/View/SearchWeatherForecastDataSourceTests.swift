//
//  SearchWeatherForecastDataSourceTests.swift
//  howweatherTests
//
//  Created by Kieu Anh Huy on 23/5/21.
//

import XCTest
@testable import howweather

class SearchWeatherForecastDataSourceTests: XCTestCase {

    var dataSource: SearchWeatherForecastDataSource!
    var tableView: UITableView!

    override func setUpWithError() throws {
        tableView = UITableView()
        tableView.register(LoadingTableViewCell.self,
                           forCellReuseIdentifier: AppConstant.SearchWeatherForecast.loadingCellId)
        tableView.register(MessageTableViewCell.self,
                           forCellReuseIdentifier: AppConstant.SearchWeatherForecast.emptyCellId)
        tableView.register(WeatherForecastTableViewCell.self,
                           forCellReuseIdentifier: AppConstant.SearchWeatherForecast.forecastCellId)
        dataSource = SearchWeatherForecastDataSource()
    }

    override func tearDownWithError() throws {
        dataSource = nil
    }

    func test_NumberOfRowsInSection_ShouldReturnNumberOfCell() {
        // Given
        dataSource.updateCells([.loading, .message("")])

        // When
        let numberOfRow = dataSource.tableView(tableView, numberOfRowsInSection: 0)

        // Then
        XCTAssertEqual(numberOfRow, 2)
    }

    func test_CellForRowAtIndex_WithLoadingType_ShouldReturnLoadingCell() {
        // Given
        dataSource.updateCells([.loading])

        // When
        let indexPath = IndexPath(row: 0, section: 0)
        let cell = dataSource.tableView(tableView, cellForRowAt: indexPath)

        // Then
        let isLoadingCell = cell is LoadingTableViewCell
        XCTAssertTrue(isLoadingCell)
    }

    func test_CellForRowAtIndex_WithMessageType_ShouldReturnMessageCell() {
        // Given
        dataSource.updateCells([.message("123")])

        // When
        let indexPath = IndexPath(row: 0, section: 0)
        let cell = dataSource.tableView(tableView, cellForRowAt: indexPath)

        // Then
        let isLoadingCell = cell is MessageTableViewCell
        XCTAssertTrue(isLoadingCell)
    }

    func test_CellForRowAtIndex_WithForecastType_ShouldReturnForecastCell() {
        // Given
        let dataModel = WeatherForecastModel(date: Date(),
                                             pressure: 1,
                                             humidity: 1,
                                             weatherDescription: "",
                                             temperature: TemperatureModel(max: 1, min: 1))
        let displayModel = WeatherForecastDisplayModel(forecastModel: dataModel,
                                                       dateFormatter: DateFormatter(),
                                                       localisation: LocalizableStub())
        dataSource.updateCells([.forecast(model: displayModel)])

        // When
        let indexPath = IndexPath(row: 0, section: 0)
        let cell = dataSource.tableView(tableView, cellForRowAt: indexPath)

        // Then
        let isLoadingCell = cell is WeatherForecastTableViewCell
        XCTAssertTrue(isLoadingCell)
    }
}
