//
//  SearchWeatherForecastViewControllerTests.swift
//  howweatherTests
//
//  Created by Kieu Anh Huy on 23/5/21.
//

import XCTest
@testable import howweather

class SearchWeatherForecastViewControllerTests: XCTestCase {
    var controller: SearchWeatherForecastViewController<SearchWeatherForecastPresentableMock>!
    var mockPresenter: SearchWeatherForecastPresentableMock!

    override func setUpWithError() throws {
        mockPresenter = SearchWeatherForecastPresentableMock()
        controller = SearchWeatherForecastViewController(presenter: mockPresenter)
    }

    override func tearDownWithError() throws {
        controller = nil
        mockPresenter = nil
    }

    func test_LoadView_ShouldSetView() {
        // When
        controller.loadView()

        // Then
        let isViewSet = controller.customView === controller.view
        XCTAssertTrue(isViewSet)
    }

    func test_ViewDidLoad_ShouldCallAttachView() {
        // When
        controller.viewDidLoad()

        // Then
        XCTAssertTrue(mockPresenter.didCallAttachView)
    }

    func test_ReloadCells_ShouldCallUpdateView() {
        // Given
        let mockView = SearchWeatherForecastViewMock()
        controller.customView = mockView

        // When
        controller.reloadCells([])
        // Then
        XCTAssertTrue(mockView.didCallReloadData)
    }

    func test_ShowTitle_ShouldUpdateViewControllerTitle() {
        let title = "title"
        // When
        controller.showTitle(title)

        // Then
        XCTAssertEqual(controller.title, title)
    }

    func test_DidCancelSearch_ShouldCallPresenterCancel() {
        // When
        controller.didCancelSearch()

        // Then
        XCTAssertTrue(mockPresenter.didCallCancel)
    }

    func test_DidChangeSearchText_ShouldCallPresenterSearch() {
        let text = "text"
        // When
        controller.didChangeSearchText(text)

        // Then
        XCTAssertTrue(mockPresenter.didCallSearch)
        XCTAssertEqual(mockPresenter.searchText, text)
    }
}

class SearchWeatherForecastPresentableMock: SearchWeatherForecastPresentable {
    typealias View = SearchWeatherForecastViewable

    var view: SearchWeatherForecastViewable?

    var didCallAttachView = false
    var didCallSearch = false
    var didCallCancel = false
    var searchText = ""

    func search(_ text: String) {
        didCallSearch = true
        searchText = text
    }

    func cancelSearch() {
        didCallCancel = true
    }

    func attach(view: SearchWeatherForecastViewable) {
        didCallAttachView = true
    }
}

class SearchWeatherForecastViewMock: SearchWeatherForecastView {
    var didCallReloadData = false

    override func reloadData(_ cells: [WeatherForecastCellType]) {
        didCallReloadData = true
    }
}
