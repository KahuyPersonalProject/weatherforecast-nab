//
//  LoadingTableViewCellTests.swift
//  howweatherTests
//
//  Created by Kieu Anh Huy on 23/5/21.
//

import XCTest
@testable import howweather

class LoadingTableViewCellTests: XCTestCase {
    var cell: LoadingTableViewCell!

    override func setUpWithError() throws {
        cell = LoadingTableViewCell(style: .default, reuseIdentifier: "")
    }

    override func tearDownWithError() throws {
        cell = nil
    }

    func test_ShowLoading_ShouldAnimating() {
        // When
        cell.showLoading()

        // Then
        XCTAssertTrue(cell.loadingIndicator.isAnimating)
    }
}
