//
//  MessageTableViewCellTests.swift
//  howweatherTests
//
//  Created by Kieu Anh Huy on 23/5/21.
//

import XCTest
@testable import howweather

class MessageTableViewCellTests: XCTestCase {
    var cell: MessageTableViewCell!

    override func setUpWithError() throws {
        cell = MessageTableViewCell(style: .default, reuseIdentifier: "")
    }

    override func tearDownWithError() throws {
        cell = nil
    }

    func test_UpdateMessage_ShouldUpdateLabel() {
        // Given
        let message = "message"
        cell.updateMessage(message)

        // When
        XCTAssertEqual(cell.messageLabel.text, message)
    }
}
