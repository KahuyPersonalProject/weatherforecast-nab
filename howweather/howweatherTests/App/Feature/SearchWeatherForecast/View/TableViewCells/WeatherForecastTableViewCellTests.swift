//
//  WeatherForecastTableViewCellTests.swift
//  howweatherTests
//
//  Created by Kieu Anh Huy on 23/5/21.
//

import XCTest
@testable import howweather

class WeatherForecastTableViewCellTests: XCTestCase {

    var cell: WeatherForecastTableViewCell!

    override func setUpWithError() throws {
        cell = WeatherForecastTableViewCell(style: .default, reuseIdentifier: "")
    }

    override func tearDownWithError() throws {
        cell = nil
    }

    func test_UpdateCell_ShouldUpdateLabels() {
        let dataModel = WeatherForecastModel(date: Date(),
                                             pressure: 1,
                                             humidity: 1,
                                             weatherDescription: "",
                                             temperature: TemperatureModel(max: 1, min: 1))
        let displayModel = WeatherForecastDisplayModel(forecastModel: dataModel,
                                                       dateFormatter: DateFormatter(),
                                                       localisation: LocalizableStub())

        // When
        cell.updateCell(displayModel)

        // Then
        XCTAssertEqual(cell.dateLabel.text, LocalisationKey.SearchWeatherForecast.forecastDate)
        XCTAssertEqual(cell.humidityLabel.text, LocalisationKey.SearchWeatherForecast.forecastHumidity)
        XCTAssertEqual(cell.temperatureLabel.text, LocalisationKey.SearchWeatherForecast.forecastAverageTemperature)
        XCTAssertEqual(cell.descriptionLabel.text, LocalisationKey.SearchWeatherForecast.forecastDescription)
        XCTAssertEqual(cell.pressureLabel.text, LocalisationKey.SearchWeatherForecast.forecastPressure)
    }
}
