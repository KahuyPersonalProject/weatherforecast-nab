//
//  SearchWeatherForecastInteractorTests.swift
//  howweatherTests
//
//  Created by Kieu Anh Huy on 23/5/21.
//

import XCTest
@testable import howweather

class SearchWeatherForecastInteractorTests: XCTestCase {
    var interactor: SearchWeatherForecastInteractor!
    var serviceMock: WeatherForecastServicingMock!

    override func setUpWithError() throws {
        serviceMock = WeatherForecastServicingMock()
        interactor = SearchWeatherForecastInteractor(forecastService: serviceMock)
    }

    override func tearDownWithError() throws {
        interactor = nil
        serviceMock = nil
    }

    func test_SearchWeatherForecastForTerm_WithError_ShouldReturnSameError() {
        // Given
        let searchText = "1234"
        serviceMock.expectResult = .failure(AppError.genericError(error: nil))

        // When
        var responseResult: Result<[WeatherForecastModel], Error>?
        interactor.searchWeatherForecastForTerm(searchText,
                                                completion: { result in
                                                    responseResult = result
                                                })

        // Then
        XCTAssertNotNil(responseResult)
        XCTAssertEqual(serviceMock.searchedTerm?.appId, AppConstant.Common.forecastAppId)
        XCTAssertEqual(serviceMock.searchedTerm?.cityName, searchText)
        XCTAssertEqual(serviceMock.searchedTerm?.numberForecastDays, AppConstant.SearchWeatherForecast.defaultNumberDay)
        XCTAssertEqual(serviceMock.searchedTerm?.unit, .celsius)
        if case .failure(let error) = responseResult,
           case AppError.genericError = error {
            XCTAssertTrue(true)
        } else {
            XCTAssertTrue(false)
        }
    }

    func test_SearchWeatherForecastForTerm_WithSuccess_ShouldReturnMappedData() {
        // Given
        let searchText = "1234"
        let temperature = TemperatureResponseModel(min: 1, max: 1)
        let weatherInfo = WeatherInfoResponseModel(description: "")
        let responseForecastModel = ForecastDateResponseModel(date: Date(),
                                                              pressure: 1,
                                                              humidity: 1,
                                                              weather: [weatherInfo],
                                                              temperature: temperature)
        let responseModel = WeatherForecastResponseModel(forecastList: [responseForecastModel])
        serviceMock.expectResult = .success(responseModel)

        // When
        var responseResult: Result<[WeatherForecastModel], Error>?
        interactor.searchWeatherForecastForTerm(searchText,
                                                completion: { result in
                                                    responseResult = result
                                                })

        // Then
        XCTAssertNotNil(responseResult)
        if case .success(let list) = responseResult,
           let firstItem = list.first {
            XCTAssertEqual(firstItem.humidity, responseForecastModel.humidity)
            XCTAssertEqual(firstItem.pressure, responseForecastModel.pressure)
            XCTAssertEqual(firstItem.weatherDescription, weatherInfo.description)
            XCTAssertEqual(firstItem.humidity, 1)
        } else {
            XCTAssertTrue(false)
        }
    }
}

class WeatherForecastServicingMock: WeatherForecastServicing {
    var numberSearchWeatherCalled = 0
    var searchedTerm: WeatherForecastRequestModel?
    var expectResult: Result<WeatherForecastResponseModel, Error>?

    func searchWeatherForecastForTerm(_ term: WeatherForecastRequestModel,
                                      completion: @escaping (Result<WeatherForecastResponseModel, Error>) -> Void) {
        numberSearchWeatherCalled += 1
        searchedTerm = term
        if let expectedResult = expectResult {
            completion(expectedResult)
        }
    }
}
