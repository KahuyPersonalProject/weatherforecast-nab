//
//  JSONParserTests.swift
//  howweatherTests
//
//  Created by Kieu Anh Huy on 24/5/21.
//

import XCTest
@testable import howweather

class JSONParserTests: XCTestCase {

    var parser: JSONParser!

    override func setUpWithError() throws {
        parser = JSONParser(jsonDecoder: JSONDecoder())
    }

    override func tearDownWithError() throws {
        parser = nil
    }

    func test_Parse_WithValidData_ShouldReturnData() {
        let value = "textValue"
        let rawData = "{\"text\": \"\(value)\"}"
        let testData = rawData.data(using: .utf8)!
        let data: DataObject? = try? parser.parse(data: testData)
        XCTAssertNotNil(data)
        XCTAssertEqual(data?.text, value)
    }

    func test_Parse_WithIncorrectJsonObject_ShouldThrowParseFailed() {
        let data = Data()
        XCTAssertThrowsError(try self.parser.parse(data: data) as DataObject, "") { _ in
            XCTAssertTrue(true)
        }
    }

    func test_Parse_WithIncorrectDataType_ShouldThrowParseFailed() {
        let value = 1
        let rawData = "{\"text\": \(value)}"
        let testData = rawData.data(using: .utf8)!
        XCTAssertThrowsError(try self.parser.parse(data: testData) as DataObject, "") { _ in
            XCTAssertTrue(true)
        }
    }
}

struct DataObject: Decodable {
    var text: String
}
