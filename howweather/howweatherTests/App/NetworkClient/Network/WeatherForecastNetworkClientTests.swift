//
//  WeatherForecastNetworkClientTests.swift
//  howweatherTests
//
//  Created by Kieu Anh Huy on 24/5/21.
//

import XCTest
@testable import howweather

class WeatherForecastNetworkClientTests: XCTestCase {

    var weatherNetworkClient: WeatherForecastNetworkClient!
    var mockNetworking: NetworkingMock!
    var mockDataParser: DataParserMock!

    override func setUpWithError() throws {
        mockNetworking = NetworkingMock()
        mockDataParser = DataParserMock()
        weatherNetworkClient = WeatherForecastNetworkClient(networkClient: mockNetworking,
                                                            dataParser: mockDataParser)
    }

    override func tearDownWithError() throws {
        weatherNetworkClient = nil
        mockDataParser = nil
        mockNetworking = nil
    }

    func test_Get_WithResponseAndParseDataSuccess_ShouldReturnModel() {
        // Given
        mockNetworking.expectResponse = .success(Data())
        mockDataParser.expectData = NetworkTestModel(text: "")

        // When
        var responseResult: Result<NetworkTestModel, AppError>?
        weatherNetworkClient.get(path: "",
                                 requestParameter: nil,
                                 completion: {(response: Result<NetworkTestModel, AppError>) in
                                    responseResult = response
                                 })
        XCTAssertEqual(mockDataParser.numberParseCalled, 1)
        if case .success = responseResult {
            XCTAssertTrue(true)
        } else {
            XCTAssertTrue(false)
        }
    }

    func test_Get_WithResponseAndParseDataFailed_ShouldReturnGenericError() {
        // Given
        mockNetworking.expectResponse = .success(Data())
        mockDataParser.expectError = AppError.genericError(error: nil)

        // When
        var responseResult: Result<NetworkTestModel, AppError>?
        weatherNetworkClient.get(path: "",
                                 requestParameter: nil,
                                 completion: {(response: Result<NetworkTestModel, AppError>) in
                                    responseResult = response
                                 })

        if case .failure(let error) = responseResult,
           case .genericError = error {
            XCTAssertTrue(true)
        } else {
            XCTAssertTrue(false)
        }
    }

    func test_Get_WithNoNetworkConnectionError_ShouldReturnAppError() {
        // Given
        mockNetworking.expectResponse = .failure(NetworkClientError.noNetworkConnection)

        // When
        var responseResult: Result<NetworkTestModel, AppError>?
        weatherNetworkClient.get(path: "",
                                 requestParameter: nil,
                                 completion: {(response: Result<NetworkTestModel, AppError>) in
                                    responseResult = response
                                 })

        if case .failure(let error) = responseResult,
           case .noNetworkConnection = error {
            XCTAssertTrue(true)
        } else {
            XCTAssertTrue(false)
        }
    }

    func test_Get_WithTimeoutError_ShouldReturnAppError() {
        // Given
        mockNetworking.expectResponse = .failure(NetworkClientError.timeout)

        // When
        var responseResult: Result<NetworkTestModel, AppError>?
        weatherNetworkClient.get(path: "",
                                 requestParameter: nil,
                                 completion: {(response: Result<NetworkTestModel, AppError>) in
                                    responseResult = response
                                 })

        if case .failure(let error) = responseResult,
           case .timeout = error {
            XCTAssertTrue(true)
        } else {
            XCTAssertTrue(false)
        }
    }

    func test_Get_WithHttpErrorAndData_ShouldReturnAppError() {
        // Given
        let rawData = "{\"cod\": \"404\", \"message\": \"message\"}"
        let testData = rawData.data(using: .utf8)!
        mockNetworking.expectResponse = .failure(NetworkClientError.httpError(404, "message", testData))
        mockDataParser.expectData = ErrorResponseModel(code: "404", message: "message")

        // When
        var responseResult: Result<NetworkTestModel, AppError>?
        weatherNetworkClient.get(path: "",
                                 requestParameter: nil,
                                 completion: {(response: Result<NetworkTestModel, AppError>) in
                                    responseResult = response
                                 })

        XCTAssertEqual(mockDataParser.numberParseCalled, 1)
        if case .failure(let error) = responseResult,
           case .httpError(let statusCode, let message, let errorModel) = error {
            XCTAssertEqual(statusCode, 404)
            XCTAssertEqual(message, "message")
            XCTAssertEqual(errorModel?.code, "404")
        } else {
            XCTAssertTrue(false)
        }
    }

    func test_Get_WithUnknowError_ShouldReturnAppError() {
        // Given
        mockNetworking.expectResponse = .failure(NetworkClientError.invalidUrl(""))

        // When
        var responseResult: Result<NetworkTestModel, AppError>?
        weatherNetworkClient.get(path: "",
                                 requestParameter: nil,
                                 completion: {(response: Result<NetworkTestModel, AppError>) in
                                    responseResult = response
                                 })

        if case .failure(let error) = responseResult,
           case .genericError = error {
            XCTAssertTrue(true)
        } else {
            XCTAssertTrue(false)
        }
    }
}

class NetworkingMock: Networking {
    var path = ""
    var method: RequestMethod?
    var parameterEncoder: ParameterEncoding?
    var requestParameter: [String: Any]?
    var expectResponse: Result<Data, Error>?
    var numberRequestCalled = 0
    func request(path: String,
                 method: RequestMethod,
                 parameterEncoder: ParameterEncoding,
                 requestParameter: [String: Any]?,
                 headers: [String: String]? = nil,
                 completion: @escaping (Result<Data, Error>) -> Void) {
        self.path = path
        self.method = method
        self.parameterEncoder = parameterEncoder
        self.requestParameter = requestParameter
        numberRequestCalled += 1
        if let result = expectResponse {
            completion(result)
        }
    }
}

class DataParserMock: DataParser {
    var expectData: Any?
    var expectError: Error?
    var numberParseCalled = 0
    func parse<M>(data: Data) throws -> M where M: Decodable {
        numberParseCalled += 1
        if let expectData = self.expectData as? M {
            return expectData
        }

        if let error = expectError {
            throw error
        }

        throw AppError.genericError(error: nil)
    }
}

struct NetworkTestModel: Decodable {
    var text: String
}
