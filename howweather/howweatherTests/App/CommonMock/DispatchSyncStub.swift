//
//  DispatchSyncStub.swift
//  howweatherTests
//
//  Created by Kieu Anh Huy on 23/5/21.
//

import Foundation
@testable import howweather

class DispatchSyncStub: Dispatcher {}

extension DispatchSyncStub: Dispatching {
    func dispatch(_ work: @escaping () -> Void) {
        queue.sync(execute: work)
    }

    func dispatchAfter(deadline: DispatchTime, workItem: DispatchWorkItem) {
        queue.sync(execute: workItem)
    }
}

extension DispatchSyncStub {
    static let main: DispatchSyncStub = DispatchSyncStub(queue: .main)
    static let global: DispatchSyncStub = DispatchSyncStub(queue: .global())
    static let background: DispatchSyncStub = DispatchSyncStub(queue: .global(qos: .background))
}
