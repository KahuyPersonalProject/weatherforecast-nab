//
//  LocalizableMock.swift
//  howweatherTests
//
//  Created by Kieu Anh Huy on 23/5/21.
//

import Foundation
import howweather

class LocalizableStub: Localizable {
    func localize(key: String) -> String {
        return key
    }
}
