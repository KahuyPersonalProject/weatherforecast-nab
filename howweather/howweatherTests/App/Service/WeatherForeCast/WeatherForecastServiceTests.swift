//
//  WeatherForecastServiceTests.swift
//  howweatherTests
//
//  Created by Kieu Anh Huy on 23/5/21.
//

import XCTest
@testable import howweather

class WeatherForecastServiceTests: XCTestCase {
    var service: WeatherForecastService!
    var mockNetworking: WeatherForecastNetworkingMock<WeatherForecastResponseModel>!
    override func setUpWithError() throws {
        mockNetworking = WeatherForecastNetworkingMock()
        service = WeatherForecastService(networkClient: mockNetworking)
    }

    override func tearDownWithError() throws {
        service = nil
        mockNetworking = nil
    }

    func test_SearchWeatherForecastForTerm_WithEmptyListSuccess_ShouldReturnForecastNotFound() {
        // Given
        let responseModel = WeatherForecastResponseModel(forecastList: [])
        mockNetworking.expectResult = .success(responseModel)
        let requestModel = WeatherForecastRequestModel(cityName: "123",
                                                       numberForecastDays: 7,
                                                       appId: "",
                                                       unit: .celsius)
        // When
        var responseResult: Result<WeatherForecastResponseModel, Error>?
        service.searchWeatherForecastForTerm(requestModel,
                                             completion: { result in
                                                responseResult = result
                                             })
        XCTAssertEqual(mockNetworking.pathCalled, ServiceConstant.WeatherForeCastApi.forecast)
        if case .failure(let error) = responseResult,
           case WeatherForecastError.forecastNotFound = error {
            XCTAssertTrue(true)
        } else {
            XCTAssertTrue(false)
        }
    }

    func test_SearchWeatherForecastForTerm_WithListSuccess_ShouldReturnSuccess() {
        // Given
        let forecastModel = ForecastDateResponseModel(date: Date(),
                                                      pressure: 1,
                                                      humidity: 1,
                                                      weather: [],
                                                      temperature: TemperatureResponseModel(min: 1, max: 1))
        let responseModel = WeatherForecastResponseModel(forecastList: [forecastModel])
        mockNetworking.expectResult = .success(responseModel)
        let requestModel = WeatherForecastRequestModel(cityName: "123",
                                                       numberForecastDays: 7,
                                                       appId: "",
                                                       unit: .celsius)
        // When
        var responseResult: Result<WeatherForecastResponseModel, Error>?
        service.searchWeatherForecastForTerm(requestModel,
                                             completion: { result in
                                                responseResult = result
                                             })

        if case .success(let model) = responseResult {
            XCTAssertEqual(model.forecastList.count, 1)
        } else {
            XCTAssertTrue(false)
        }
    }

    func test_SearchWeatherForecastForTerm_WithNotFoundFailure_ShouldReturnForecastNotFound() {
        // Given
        let error = ErrorResponseModel(code: "404", message: "")
        mockNetworking.expectResult = .failure(AppError.httpError(404, "", error))
        let requestModel = WeatherForecastRequestModel(cityName: "123",
                                                       numberForecastDays: 7,
                                                       appId: "",
                                                       unit: .celsius)
        // When
        var responseResult: Result<WeatherForecastResponseModel, Error>?
        service.searchWeatherForecastForTerm(requestModel,
                                             completion: { result in
                                                responseResult = result
                                             })

        if case .failure(let error) = responseResult,
           case WeatherForecastError.forecastNotFound = error {
            XCTAssertTrue(true)
        } else {
            XCTAssertTrue(false)
        }
    }
}

class WeatherForecastNetworkingMock<R: Decodable>: WeatherForecastNetworking {
    var pathCalled: String = ""
    var requestParameter: [String: Any]?
    var expectResult: Result<R, AppError>?
    func get<R>(path: String,
                requestParameter: [String: Any]?,
                completion: @escaping (Result<R, AppError>) -> Void) where R: Decodable {
        self.pathCalled = path
        self.requestParameter = requestParameter
        if let expectResult = expectResult as? Result<R, AppError> {
            completion(expectResult)
        }
    }
}
