//
//  AcceptanceTestURLProtocol.swift
//  howweather
//
//  Created by Kieu Anh Huy on 24/5/21.
//

import Foundation
#if ACCEPTANCETEST
class AcceptanceTestURLProtocol: URLProtocol {
    override class func canInit(with request: URLRequest) -> Bool {
        return true
    }

    override class func canonicalRequest(for request: URLRequest) -> URLRequest {
        return request
    }

    override class func requestIsCacheEquivalent(_ lhs: URLRequest, to rhs: URLRequest) -> Bool {
        return false
    }

    override func startLoading() {
        let result = stubRequestHandler(request)
        if let data = result.data {
            client?.urlProtocol(self, didReceive: result.response, cacheStoragePolicy: .notAllowed)
            client?.urlProtocol(self, didLoad: data)
            client?.urlProtocolDidFinishLoading(self)
        } else {
            client?.urlProtocol(self, didFailWithError: result.error!)
        }
    }

    override func stopLoading() {
    }

    private func stubRequestHandler(_ request: URLRequest) -> RequestHandlerResult {
        let path = request.url?.path ?? ""
        var requestHandler: RequestHandlerStub?
        switch path {
        case ServiceConstant.WeatherForeCastApi.forecast:
            requestHandler = ForecastRequestHandlerStub()
        default:
            break
        }
        let defaultResponse = HTTPURLResponse(url: request.url!,
                                       statusCode: 200,
                                       httpVersion: nil,
                                       headerFields: nil)!
        if let requestHandler = requestHandler {
            return requestHandler.handleRequest(request)
        }
        return RequestHandlerResult(response: defaultResponse,
                                    data: nil,
                                    error: nil)
    }
}
#endif
