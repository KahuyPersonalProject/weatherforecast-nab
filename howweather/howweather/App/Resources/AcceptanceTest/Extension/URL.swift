//
//  URL.swift
//  howweather
//
//  Created by Kieu Anh Huy on 24/5/21.
//

import Foundation
#if ACCEPTANCETEST
extension URL {
    var queryParameters: [String: String]? {
        guard let urlComponent = URLComponents(url: self, resolvingAgainstBaseURL: false),
              let queryItems = urlComponent.queryItems
        else { return nil }
        let parameters = queryItems.reduce([String: String](), { (accumulatedData, queryItem) -> [String: String] in
            guard let value = queryItem.value else { return accumulatedData }
            var result = accumulatedData
            result[queryItem.name] = value
            return result
        })

        return parameters
    }
}
#endif
