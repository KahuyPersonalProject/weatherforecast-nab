//
//  RequestHandlerStub.swift
//  howweather
//
//  Created by Kieu Anh Huy on 24/5/21.
//

import Foundation

#if ACCEPTANCETEST
protocol RequestHandlerStub {
    func handleRequest(_ request: URLRequest) -> RequestHandlerResult
}

struct RequestHandlerResult {
    let response: HTTPURLResponse
    let data: Data?
    let error: Error?
}
#endif
