//
//  ForecastRequestHandlerStub.swift
//  howweather
//
//  Created by Kieu Anh Huy on 24/5/21.
//

import Foundation

#if ACCEPTANCETEST
class ForecastRequestHandlerStub: RequestHandlerStub {
    private enum Scenario: String {
        case validSearchText = "Mel"
        case notFoundSearchText = "NotFound"
        case networkTimeout = "networkTimeout"
        case noNetworkConnection = "noNetworkConnection"
        case unknownError = "unknownError"

        var stubDataFileName: String {
            switch self {
            case .validSearchText:
                return "WeatherForecaseValidData"
            case .notFoundSearchText:
                return "WeatherForecastNotFound"
            default:
                return ""
            }
        }
    }

    func handleRequest(_ request: URLRequest) -> RequestHandlerResult {
        var response = HTTPURLResponse(url: request.url!,
                                       statusCode: 200,
                                       httpVersion: nil,
                                       headerFields: nil)!
        var scenario = Scenario.notFoundSearchText
        if let parameters = request.url?.queryParameters,
           let termQuery = parameters["q"],
           let scenarioFromQuery = Scenario(rawValue: termQuery) {
            scenario = scenarioFromQuery
        }
        var data: Data?
        var error: Error?
        switch scenario {
        case .networkTimeout:
            response = HTTPURLResponse(url: request.url!,
                                       statusCode: URLError.timedOut.rawValue,
                                       httpVersion: nil,
                                       headerFields: nil)!
            error = NSError(domain: "",
                            code: URLError.timedOut.rawValue,
                            userInfo: nil)
        case .noNetworkConnection:
            response = HTTPURLResponse(url: request.url!,
                                       statusCode: URLError.notConnectedToInternet.rawValue,
                                       httpVersion: nil,
                                       headerFields: nil)!
            error = NSError(domain: "",
                            code: URLError.notConnectedToInternet.rawValue,
                            userInfo: nil)
        case .unknownError:
            response = HTTPURLResponse(url: request.url!,
                                       statusCode: URLError.unknown.rawValue,
                                       httpVersion: nil,
                                       headerFields: nil)!
            error = NSError(domain: "",
                            code: URLError.unknown.rawValue,
                            userInfo: nil)
        case .validSearchText:
            data = loadDataFromFile(name: scenario.stubDataFileName, ofType: "json")
        case .notFoundSearchText:
            response = HTTPURLResponse(url: request.url!,
                                       statusCode: 404,
                                       httpVersion: nil,
                                       headerFields: nil)!
            data = loadDataFromFile(name: scenario.stubDataFileName, ofType: "json")
        }

        return RequestHandlerResult(response: response,
                                    data: data,
                                    error: error)
    }

    private func loadDataFromFile(name: String,
                                  ofType type: String) -> Data? {
        guard let filepath = Bundle.main.path(forResource: name,
                                              ofType: type)
        else {
            return nil
        }
        let contents = try? String(contentsOfFile: filepath)
        return contents?.data(using: .utf8)
    }
}
#endif
