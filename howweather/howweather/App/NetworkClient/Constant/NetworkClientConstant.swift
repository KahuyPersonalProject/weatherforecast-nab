//
//  NetworkClientConstant.swift
//  howweather
//
//  Created by Kieu Anh Huy on 23/5/21.
//

import Foundation

extension AppConstant {
    enum Networking {
        static let memoryCapacity = 4 * 1024 * 1024
        static let diskCapacity = 10 * 1024 * 1024
        static let requestTimeout: TimeInterval = 30
        static let cacheTimeout: TimeInterval = 10 * 60 // Cache response in 10 min
    }
}
