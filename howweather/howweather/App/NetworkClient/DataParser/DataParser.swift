//
//  DataParser.swift
//  howweather
//
//  Created by Kieu Anh Huy on 22/5/21.
//

import Foundation

protocol DataParser {
    func parse<M: Decodable>(data: Data) throws -> M
}
