//
//  JSONParser.swift
//  howweather
//
//  Created by Kieu Anh Huy on 22/5/21.
//

import Foundation

class JSONParser: DataParser {
    let jsonDecoder: JSONDecoder

    init(jsonDecoder: JSONDecoder) {
        self.jsonDecoder = jsonDecoder
    }

    func parse<M: Decodable>(data: Data) throws -> M {
        return try jsonDecoder.decode(M.self, from: data)
    }
}
