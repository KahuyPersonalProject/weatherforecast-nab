//
//  WeatherForecastCache.swift
//  howweather
//
//  Created by Kieu Anh Huy on 22/5/21.
//

import Foundation

class WeatherForecastCache: URLCache {
    private let cacheExpiryKey = "cache_expiry"
    private let cacheExpireInterval = AppConstant.Networking.cacheTimeout

    override func storeCachedResponse(_ cachedResponse: CachedURLResponse, for dataTask: URLSessionDataTask) {
        var userInfo: [AnyHashable: Any]
        userInfo = cachedResponse.userInfo ?? [:]
        userInfo[cacheExpiryKey] = Date()
        let newCachedResponse = CachedURLResponse(response: cachedResponse.response,
                                                  data: cachedResponse.data,
                                                  userInfo: userInfo,
                                                  storagePolicy: cachedResponse.storagePolicy)
        super.storeCachedResponse(newCachedResponse, for: dataTask)
    }

    override func getCachedResponse(for dataTask: URLSessionDataTask,
                                    completionHandler: @escaping (CachedURLResponse?) -> Void) {
        super.getCachedResponse(for: dataTask,
                                completionHandler: {[weak self] cachedResponse in
                                    guard let self = self,
                                          let cachedResponse = cachedResponse
                                    else {
                                        completionHandler(nil)
                                        return
                                    }
                                    if let userInfo = cachedResponse.userInfo,
                                       let cacheDate = userInfo[self.cacheExpiryKey] as? Date {
                                        let count = cachedResponse.data.count
                                        if (cacheDate.timeIntervalSinceNow < -self.cacheExpireInterval) || count == 0 {
                                            self.removeCachedResponse(for: dataTask)
                                            completionHandler(nil)
                                            return
                                        }
                                    }
                                    completionHandler(cachedResponse)
                                })
    }
}
