//
//  Networking.swift
//  howweather
//
//  Created by Kieu Anh Huy on 22/5/21.
//

import Foundation

protocol Networking {
    func request(path: String,
                 method: RequestMethod,
                 parameterEncoder: ParameterEncoding,
                 requestParameter: [String: Any]?,
                 headers: [String: String]?,
                 completion: @escaping (Result<Data, Error>) -> Void)
}

// Create an simple Adapter. If we change network client will minimum code change
extension NetworkClient: Networking {
}
