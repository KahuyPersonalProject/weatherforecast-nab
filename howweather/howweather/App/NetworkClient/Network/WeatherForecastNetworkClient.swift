//
//  WeatherForecastNetworkClient.swift
//  howweather
//
//  Created by Kieu Anh Huy on 22/5/21.
//

import Foundation

class WeatherForecastNetworkClient: WeatherForecastNetworking {
    let networkClient: Networking
    let dataParser: DataParser

    init(networkClient: Networking,
         dataParser: DataParser) {
        self.networkClient = networkClient
        self.dataParser = dataParser
    }

    func get<M: Decodable>(path: String,
                           requestParameter: [String: Any]?,
                           completion: @escaping (Result<M, AppError>) -> Void) {
        networkClient.request(path: path,
                              method: .get,
                              parameterEncoder: ParameterEncoder.queryString,
                              requestParameter: requestParameter,
                              headers: nil,
                              completion: {[weak self] result  in
                                guard let self = self else { return }
                                let handledResult: Result<M, AppError> = self.handleGetResult(result)
                                completion(handledResult)
                              })
    }

    private func handleGetResult<M: Decodable>(_ result: Result<Data, Error>) -> Result<M, AppError> {
        let mappedErrorResult = mapError(result: result)
        switch mappedErrorResult {
        case .success(let data):
           return mappedData(data)
        case .failure(let error):
            return .failure(error)
        }
    }

    private func mapError(result: Result<Data, Error>) -> Result<Data, AppError> {
        let errorMappedResult = result.mapError { error -> AppError in
            switch error {
            case NetworkClientError.noNetworkConnection:
                return AppError.noNetworkConnection
            case NetworkClientError.timeout:
                return AppError.timeout
            case NetworkClientError.httpError(let statusCode, let message, let errorData):
                return handleHttpError(statusCode: statusCode, message: message, errorData: errorData)
            default:
                return AppError.genericError(error: error)
            }
        }

        return errorMappedResult
    }

    private func handleHttpError(statusCode: Int,
                                 message: String,
                                 errorData: Data?) -> AppError {
        if let errorData = errorData {
            let errorResponseResult: Result<ErrorResponseModel, AppError>
            errorResponseResult = mappedData(errorData)
            switch errorResponseResult {
            case .success(let errorResponse):
                return AppError.httpError(statusCode, message, errorResponse)
            case .failure(let error):
                return error
            }
        }
        return AppError.httpError(statusCode, message, nil)
    }

    private func mappedData<M: Decodable>(_ data: Data) -> Result<M, AppError> {
        do {
            let responseModel: M = try self.dataParser.parse(data: data)
            return .success(responseModel)
        } catch {
            return .failure(AppError.genericError(error: error))
        }
    }
}
