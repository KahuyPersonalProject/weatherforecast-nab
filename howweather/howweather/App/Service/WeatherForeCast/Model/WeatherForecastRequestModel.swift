//
//  WeatherForecastRequestModel.swift
//  howweather
//
//  Created by Kieu Anh Huy on 22/5/21.
//

import Foundation

struct WeatherForecastRequestModel {
    let cityName: String
    let numberForecastDays: Int
    let appId: String
    let unit: ForecastUnit
}

extension WeatherForecastRequestModel: RequestParamenter {
    var parameters: [String: Any] {
        return ["q": cityName,
                "cnt": numberForecastDays,
                "appid": appId,
                "units": unit.requestUnit]
    }
}
