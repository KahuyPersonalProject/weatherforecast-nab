//
//  WeatherForecastResponseModel.swift
//  howweather
//
//  Created by Kieu Anh Huy on 22/5/21.
//

import Foundation

struct WeatherForecastResponseModel: Decodable {
    var forecastList: [ForecastDateResponseModel]
    enum CodingKeys: String, CodingKey {
        case forecastList = "list"
    }
}

struct ForecastDateResponseModel: Decodable {
    var date: Date
    var pressure: Int
    var humidity: Int
    var weather: [WeatherInfoResponseModel]
    var temperature: TemperatureResponseModel

    enum CodingKeys: String, CodingKey {
        case date = "dt"
        case temperature = "temp"
        case pressure
        case humidity
        case weather
    }
}

struct WeatherInfoResponseModel: Decodable {
    var description: String

    enum CodingKeys: String, CodingKey {
        case description
    }
}

struct TemperatureResponseModel: Decodable {
    var min: Float
    var max: Float

    enum CodingKeys: String, CodingKey {
        case min, max
    }
}
