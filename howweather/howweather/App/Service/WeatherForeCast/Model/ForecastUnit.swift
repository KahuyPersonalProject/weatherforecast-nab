//
//  ForecastUnit.swift
//  howweather
//
//  Created by Kieu Anh Huy on 23/5/21.
//

import Foundation

enum ForecastUnit {
    case fahrenheit
    case celsius
}

extension ForecastUnit {
    var requestUnit: String {
        switch self {
        case .celsius:
            return ServiceConstant.WeatherForeCastApi.metricUnit
        case .fahrenheit:
            return ServiceConstant.WeatherForeCastApi.imperialUnit
        }
    }
}
