//
//  WeatherForecastError.swift
//  howweather
//
//  Created by Kieu Anh Huy on 23/5/21.
//

import Foundation

enum WeatherForecastError: String, Error {
    case forecastNotFound = "404"
}
