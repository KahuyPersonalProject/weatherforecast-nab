//
//  WeatherForecastService.swift
//  howweather
//
//  Created by Kieu Anh Huy on 22/5/21.
//

import Foundation

class WeatherForecastService: WeatherForecastServicing {

    let networkClient: WeatherForecastNetworking

    init(networkClient: WeatherForecastNetworking) {
        self.networkClient = networkClient
    }

    func searchWeatherForecastForTerm(_ term: WeatherForecastRequestModel,
                                      completion: @escaping (Result<WeatherForecastResponseModel, Error>) -> Void) {
        let forecastApi = ServiceConstant.WeatherForeCastApi.forecast
        networkClient.get(path: forecastApi,
                          requestParameter: term.parameters,
                          completion: {[weak self] (result: Result<WeatherForecastResponseModel, AppError>) in
                            guard let self = self else { return }
                            let mappedResult = result.mapError {error -> Error in
                                return self.mapBusinessError(error)
                            }
                            if case .success(let model) = mappedResult,
                               model.forecastList.isEmpty {
                                completion(.failure(WeatherForecastError.forecastNotFound))
                                return
                            }
                            completion(mappedResult)
                          })
    }

    private func mapBusinessError(_ error: AppError) -> Error {
        guard case AppError.httpError(_, _, let data?) = error,
              let forecastError = WeatherForecastError(rawValue: data.code)
        else { return error }
        return forecastError
    }
}
