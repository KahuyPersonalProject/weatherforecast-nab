//
//  WeatherForecastServicing.swift
//  howweather
//
//  Created by Kieu Anh Huy on 22/5/21.
//

import Foundation

protocol WeatherForecastServicing {
    func searchWeatherForecastForTerm(_ term: WeatherForecastRequestModel,
                                      completion: @escaping (Result<WeatherForecastResponseModel, Error>) -> Void)
}
