//
//  WeatherForecastNetworking.swift
//  howweather
//
//  Created by Kieu Anh Huy on 22/5/21.
//

import Foundation

protocol WeatherForecastNetworking {
    func get<M: Decodable>(path: String,
                           requestParameter: [String: Any]?,
                           completion: @escaping (Result<M, AppError>) -> Void)
}
