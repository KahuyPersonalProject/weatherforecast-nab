//
//  ServiceConstant.swift
//  howweather
//
//  Created by Kieu Anh Huy on 22/5/21.
//

import Foundation

enum ServiceConstant {
    enum WeatherForeCastApi {
        static let forecast = "/data/2.5/forecast/daily"
        static let metricUnit = "metric"
        static let imperialUnit = "imperial"
    }
}
