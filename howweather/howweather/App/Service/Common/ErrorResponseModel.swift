//
//  ErrorResponseModel.swift
//  howweather
//
//  Created by Kieu Anh Huy on 23/5/21.
//

import Foundation

struct ErrorResponseModel: Decodable {
    let code: String
    let message: String

    enum CodingKeys: String, CodingKey {
        case code = "cod"
        case message
    }
}
