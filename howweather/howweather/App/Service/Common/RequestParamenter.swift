//
//  RequestParamenter.swift
//  howweather
//
//  Created by Kieu Anh Huy on 22/5/21.
//

import Foundation

protocol RequestParamenter {
    var parameters: [String: Any] { get }
}
