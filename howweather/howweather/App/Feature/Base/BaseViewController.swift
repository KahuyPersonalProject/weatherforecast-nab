//
//  BaseViewController.swift
//  howweather
//
//  Created by Kieu Anh Huy on 22/5/21.
//

import UIKit

class BaseViewController<P: Presentable>: UIViewController {
    var presenter: P
    init(presenter: P) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        if let view = self as? P.View {
            presenter.attach(view: view)
            return
        }
    }

    deinit {
        presenter.detach()
    }
}
