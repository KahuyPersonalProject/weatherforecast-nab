//
//  Presentable.swift
//  howweather
//
//  Created by Kieu Anh Huy on 22/5/21.
//

import Foundation

protocol Presentable: AnyObject {
    associatedtype View
    var view: View? { get set }
    func attach(view: View)
    func detach()
}

extension Presentable {
    func detach() {
        self.view = nil
    }
}
