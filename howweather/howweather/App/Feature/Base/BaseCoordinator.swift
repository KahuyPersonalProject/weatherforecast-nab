//
//  BaseCoordinator.swift
//  howweather
//
//  Created by Kieu Anh Huy on 22/5/21.
//

import Foundation
import UIKit

protocol Coordinator: AnyObject {
    associatedtype ResultType
    var identifier: UUID { get }
    func start(ready: ((UIViewController) -> Void)?,
               completion: @escaping ((UIViewController, ResultType) -> Void))
}

class BaseCoordinator<CoordinatorResult>: Coordinator {
    typealias ResultType = CoordinatorResult
    let identifier = UUID()
    var completion: ((UIViewController, ResultType) -> Void)?
    var childCoordinators = [UUID: Any]()
    let depedencyContainer: Dependency?

    public init(depedencyContainer: Dependency? = nil) {
        self.depedencyContainer = depedencyContainer
    }

    open func coordinate<C: Coordinator>(to coordinator: C,
                                         ready: ((UIViewController) -> Void)?,
                                         completion: @escaping ((UIViewController, C.ResultType) -> Void)) {
        store(coordinator: coordinator)
        coordinator.start(ready: ready,
                          completion: {[weak self] (viewController, result) -> Void   in
                            guard let self = self else { return }
                            completion(viewController, result)
                            self.release(coordinator)
                          })
    }

    open func start(ready: ((UIViewController) -> Void)?,
                    completion: @escaping ((UIViewController, ResultType) -> Void)) {
        self.completion = completion
    }

    public func release<C: Coordinator>(_ coordinator: C) {
        childCoordinators.removeValue(forKey: coordinator.identifier)
    }

    private func store<C: Coordinator>(coordinator: C) {
        childCoordinators[coordinator.identifier] = coordinator
    }
}
