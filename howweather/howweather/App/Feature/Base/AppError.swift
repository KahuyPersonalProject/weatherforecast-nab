//
//  WeatherForecastServiceError.swift
//  howweather
//
//  Created by Kieu Anh Huy on 23/5/21.
//

import Foundation

enum AppError: Error {
    case noNetworkConnection
    case timeout
    case httpError(Int, String, ErrorResponseModel?)
    case genericError(error: Error?)
}
