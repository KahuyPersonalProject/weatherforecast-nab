//
//  AppRootCoordinator.swift
//  howweather
//
//  Created by Kieu Anh Huy on 22/5/21.
//

import Foundation
import UIKit

class AppRootCoordinator: BaseCoordinator<Void> {
    let appWindow: UIWindow

    init(appWindow: UIWindow,
         rootDependencyContainer: AppDependency) {
        self.appWindow = appWindow
        super.init(depedencyContainer: rootDependencyContainer)
    }

    func start() {
        let searchWeatherForecast = SearchWeatherForecastCoordinator(depedencyContainer: depedencyContainer)
        coordinate(to: searchWeatherForecast,
                   ready: {[weak self] viewController in
                    guard let self = self else { return }
                    let navigationController = UINavigationController(rootViewController: viewController)
                    let navigationBar = navigationController.navigationBar
                    navigationBar.barTintColor = .white
                    navigationBar.shadowImage = UIImage()
                    self.appWindow.rootViewController = navigationController
                    self.appWindow.makeKeyAndVisible()
                   },
                   completion: { (_, _) in
                    // DO NOTHING
                   })
    }
}
