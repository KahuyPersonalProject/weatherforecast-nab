//
//  AppDependency.swift
//  howweather
//
//  Created by Kieu Anh Huy on 22/5/21.
//

import Foundation

class AppDependency: SimpleDependency {
    override func setup() {
        let networkingComponentDependency = NetworkingComponentDependency()
        let serviceComponentDependency = ServiceComponentDependency()
        let helperDependency = HelperComponentDependency()
        container.register(components: [networkingComponentDependency,
                                        serviceComponentDependency,
                                        helperDependency])
    }
}
