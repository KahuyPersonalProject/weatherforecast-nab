//
//  HelperComponentDependency.swift
//  howweather
//
//  Created by Kieu Anh Huy on 23/5/21.
//

import Foundation

class HelperComponentDependency: ComponentDependency {
    func register(container: DependencyContainer) {
        container.register(DispatchQueues.self,
                           factory: { container -> DispatchQueues in
                            return DispatchQueueImpl(mainDispatch: DispatchAsync.main,
                                                     backgroundDispatch: DispatchAsync.global)
                           },
                           scope: .container)

        container.register(Localizable.self,
                           factory: { container -> Localizable in
                            return DefaultLocalization(bundle: Bundle.main)
                           },
                           scope: .container)
    }
}
