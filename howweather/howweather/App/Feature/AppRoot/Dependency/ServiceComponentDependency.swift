//
//  ServiceComponentDependency.swift
//  howweather
//
//  Created by Kieu Anh Huy on 22/5/21.
//

import Foundation

class ServiceComponentDependency: ComponentDependency {
    func register(container: DependencyContainer) {
        container.register(WeatherForecastServicing.self,
                           factory: { container in
                            guard let networkClient = container.resolve(WeatherForecastNetworking.self)
                            else {
                                fatalError("Please register WeatherForecastNetworking")
                            }
                            return WeatherForecastService(networkClient: networkClient)
                           },
                           scope: Scope.container)
    }
}
