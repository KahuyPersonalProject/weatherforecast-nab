//
//  NetworkingComponentDependency.swift
//  howweather
//
//  Created by Kieu Anh Huy on 22/5/21.
//

import Foundation

class NetworkingComponentDependency: ComponentDependency {
    func register(container: DependencyContainer) {
        registerBaseNetwork(container)
        registerAppNetworking(container)
    }

    private func registerBaseNetwork(_ container: DependencyContainer) {
        container.register(Networking.self,
                           factory: { container in
                            guard let session = container.resolve(URLSession.self)
                            else {
                                fatalError("Please register Networking")
                            }
                            let baseUrl = AppConstant.Common.baseForecastUrl
                            return NetworkClient(baseUrl: baseUrl,
                                                 session: session)
                           },
                           scope: Scope.container)

        container.register(URLSession.self,
                           factory: { container in
                            guard let urlCache = container.resolve(URLCache.self)
                            else {
                                fatalError("Please register URLCache")
                            }
                            let config = URLSessionConfiguration.default
                            config.timeoutIntervalForRequest = AppConstant.Networking.requestTimeout
                            config.urlCache = urlCache
                            config.requestCachePolicy = .returnCacheDataElseLoad
                            #if ACCEPTANCETEST
                            config.protocolClasses = [AcceptanceTestURLProtocol.self]
                            #endif
                            let session = URLSession(configuration: config)
                            return session
                           },
                           scope: Scope.container)

        container.register(URLCache.self,
                           factory: { container in
                            let memoryCapacity = AppConstant.Networking.memoryCapacity
                            let diskCapacity = AppConstant.Networking.diskCapacity
                            let urlCache = WeatherForecastCache(memoryCapacity: memoryCapacity,
                                                                diskCapacity: diskCapacity)
                            return urlCache
                           },
                           scope: Scope.container)
    }

    private func registerAppNetworking(_ container: DependencyContainer) {
        container.register(WeatherForecastNetworking.self,
                           factory: { container in
                            guard let networkClient = container.resolve(Networking.self)
                            else {
                                fatalError("Please register Networking")
                            }
                            guard let parser = container.resolve(DataParser.self)
                            else {
                                fatalError("Please register JSONParser")
                            }

                            let forecastNetworkClient = WeatherForecastNetworkClient(networkClient: networkClient,
                                                                                     dataParser: parser)
                            return forecastNetworkClient
                           },
                           scope: Scope.container)

        container.register(DataParser.self,
                           factory: { container in
                            let decoder = JSONDecoder()
                            decoder.dateDecodingStrategy = .secondsSince1970
                            let parser = JSONParser(jsonDecoder: decoder)
                            return parser
                           },
                           scope: Scope.container)
    }
}
