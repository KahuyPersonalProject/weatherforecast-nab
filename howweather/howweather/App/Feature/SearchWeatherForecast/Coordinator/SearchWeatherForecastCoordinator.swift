//
//  SearchWeatherForecastCoordinator.swift
//  howweather
//
//  Created by Kieu Anh Huy on 22/5/21.
//

import Foundation
import UIKit

class SearchWeatherForecastCoordinator: BaseCoordinator<Void> {
    override func start(ready: ((UIViewController) -> Void)?,
                        completion: @escaping ((UIViewController, BaseCoordinator<Void>.ResultType) -> Void)) {
        super.start(ready: ready, completion: completion)
        guard let forecastService = depedencyContainer?.resolve(WeatherForecastServicing.self)
        else { return }
        guard let dispatchQueues = depedencyContainer?.resolve(DispatchQueues.self)
        else { return }
        guard let localisation = depedencyContainer?.resolve(Localizable.self)
        else { return }
        let interactor = SearchWeatherForecastInteractor(forecastService: forecastService)
        let presenter = SearchWeatherForecastPresenter(interactor: interactor,
                                                       dispatchQueues: dispatchQueues,
                                                       localisation: localisation)
        let viewController = SearchWeatherForecastViewController(presenter: presenter)
        ready?(viewController)
    }
}
