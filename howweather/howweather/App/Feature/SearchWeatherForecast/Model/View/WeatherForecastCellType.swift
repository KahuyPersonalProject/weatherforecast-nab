//
//  WeatherForecastCellType.swift
//  howweather
//
//  Created by Kieu Anh Huy on 23/5/21.
//

import Foundation

enum WeatherForecastCellType {
    case loading
    case message(String)
    case forecast(model: WeatherForecastDisplayable)
}
