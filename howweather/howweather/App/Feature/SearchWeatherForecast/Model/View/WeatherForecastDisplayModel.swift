//
//  WeatherForecastDisplayModel.swift
//  howweather
//
//  Created by Kieu Anh Huy on 23/5/21.
//

import Foundation

protocol WeatherForecastDisplayable {
    var displayDate: String { get }
    var displayTemperature: String { get }
    var displayPressure: String { get }
    var displayHumidity: String { get }
    var displayDescription: String { get }
}

struct WeatherForecastDisplayModel {
    let forecastModel: WeatherForecastModel
    let dateFormatter: DateFormatter
    let localisation: Localizable
}

extension WeatherForecastDisplayModel: WeatherForecastDisplayable {
    var displayDate: String {
        let textLocalisationKey = LocalisationKey.SearchWeatherForecast.forecastDate
        let displayDate = dateFormatter.string(from: forecastModel.date)
        return localise(textLocalisationKey, with: displayDate)
    }

    var displayTemperature: String {
        let textLocalisationKey = LocalisationKey.SearchWeatherForecast.forecastAverageTemperature
        let defaultUnit = AppConstant.SearchWeatherForecast.defaultUnit.displayUnit
        let averageTemperature = String(format: "%.f", forecastModel.temperature.averageTemperature)
        let displayTemperature = "\(averageTemperature)\(defaultUnit)"
        return localise(textLocalisationKey, with: displayTemperature)
    }

    var displayPressure: String {
        let textLocalisationKey = LocalisationKey.SearchWeatherForecast.forecastPressure
        return localise(textLocalisationKey, with: "\(forecastModel.pressure)")
    }

    var displayHumidity: String {
        let textLocalisationKey = LocalisationKey.SearchWeatherForecast.forecastHumidity
        return localise(textLocalisationKey, with: "\(forecastModel.humidity)%")
    }

    var displayDescription: String {
        let textLocalisationKey = LocalisationKey.SearchWeatherForecast.forecastDescription
        return localise(textLocalisationKey, with: "\(forecastModel.weatherDescription)")
    }

    private func localise(_ key: String,
                          with value: String) -> String {
        let textLocalisationKey = key
        let textFormat = localisation.localize(key: textLocalisationKey)
        return String(format: textFormat, value)
    }
}
