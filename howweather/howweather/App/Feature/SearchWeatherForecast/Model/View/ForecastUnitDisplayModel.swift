//
//  ForecastUnitDisplayable.swift
//  howweather
//
//  Created by Kieu Anh Huy on 23/5/21.
//

import Foundation

extension ForecastUnit {
    var displayUnit: String {
        switch self {
        case .celsius:
            return "°C"
        case .fahrenheit:
            return "°F"
        }
    }
}
