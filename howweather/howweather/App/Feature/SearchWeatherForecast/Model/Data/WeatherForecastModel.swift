//
//  WeatherForecastModel.swift
//  howweather
//
//  Created by Kieu Anh Huy on 22/5/21.
//

import Foundation

struct WeatherForecastModel {
    let date: Date
    let pressure: Int
    let humidity: Int
    let weatherDescription: String
    let temperature: TemperatureModel
}

extension WeatherForecastModel {
    init(responseModel: ForecastDateResponseModel) {
        let temperatures = TemperatureModel(responseModel: responseModel.temperature)
        let weatherDescription = responseModel.weather.first?.description ?? ""
        self.init(date: responseModel.date,
                  pressure: responseModel.pressure,
                  humidity: responseModel.humidity,
                  weatherDescription: weatherDescription,
                  temperature: temperatures)
    }
}
