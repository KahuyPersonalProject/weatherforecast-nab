//
//  TemperatureModel.swift
//  howweather
//
//  Created by Kieu Anh Huy on 23/5/21.
//

import Foundation

struct TemperatureModel {
    let max: Float
    let min: Float

    var averageTemperature: Float {
        return (max + min)/2
    }
}

extension TemperatureModel {
    init(responseModel: TemperatureResponseModel) {
        self.init(max: responseModel.max, min: responseModel.min)
    }
}
