//
//  SearchWeatherForecastPresenter.swift
//  howweather
//
//  Created by Kieu Anh Huy on 22/5/21.
//

import Foundation

class SearchWeatherForecastPresenter: SearchWeatherForecastPresentable {
    typealias View = SearchWeatherForecastViewable

    private let interactor: SearchWeatherForecastInteractable
    private let dispatchQueues: DispatchQueues
    private let localisation: Localizable

    weak var view: SearchWeatherForecastViewable?
    var currentSearchText: String?

    init(interactor: SearchWeatherForecastInteractable,
         dispatchQueues: DispatchQueues,
         localisation: Localizable) {
        self.interactor = interactor
        self.dispatchQueues = dispatchQueues
        self.localisation = localisation
    }

    func attach(view: SearchWeatherForecastViewable) {
        self.view = view
        let title = localisation.localize(key: LocalisationKey.SearchWeatherForecast.title)
        view.showTitle(title)
    }

    func search(_ text: String) {
        currentSearchText = text
        let shouldStartSearch = text.count >= AppConstant.SearchWeatherForecast.numberCharacterStartSearch
        guard shouldStartSearch
        else {
            showRequireInputMessage()
            return
        }
        showLoading()
        dispatchQueues.background.dispatch {[weak self] in
            self?.interactor
                .searchWeatherForecastForTerm(text,
                                              completion: {[weak self] result in
                                                guard let self = self else { return }
                                                let isResultOfCurrentText = text == self.currentSearchText
                                                guard isResultOfCurrentText else { return }
                                                self.handleSearchResult(result)
                                              })
        }
    }

    func cancelSearch() {
        currentSearchText = nil
        showRequireInputMessage()
    }

    private func showRequireInputMessage() {
        let localisationKey = LocalisationKey.SearchWeatherForecast.inputSearch
        let message = localisation.localize(key: localisationKey)
        showMessage(message)
    }

    private func handleSearchResult(_ result: Result<[WeatherForecastModel], Error>) {
        switch result {
        case .success(let data):
            showForecast(data)
        case .failure(let error):
            handleError(error)
        }
    }

    private func showForecast(_ data: [WeatherForecastModel]) {
        guard !data.isEmpty
        else {
            let localisationKey = LocalisationKey.SearchWeatherForecast.noForecastFound
            let message = localisation.localize(key: localisationKey)
            showMessage(message)
            return
        }

        let dateFormater = DateFormatter()
        dateFormater.dateFormat = AppConstant.SearchWeatherForecast.forecastDisplayDateFormat
        let cells = data.map { forecastModel -> WeatherForecastCellType in
            let forecastDisplay = WeatherForecastDisplayModel(forecastModel: forecastModel,
                                                              dateFormatter: dateFormater,
                                                              localisation: localisation)
            return WeatherForecastCellType.forecast(model: forecastDisplay)
        }
        dispatchQueues.main.dispatch {[weak self] in
            self?.view?.reloadCells(cells)
        }
    }

    private func showMessage(_ message: String) {
        dispatchQueues.main.dispatch {[weak self] in
            self?.view?.reloadCells([.message(message)])
        }
    }

    private func showLoading() {
        dispatchQueues.main.dispatch {[weak self] in
            self?.view?.reloadCells([.loading])
        }
    }

    private func handleError(_ error: Error) {
        let message: String
        switch error {
        case WeatherForecastError.forecastNotFound:
            message = localisation.localize(key: LocalisationKey.SearchWeatherForecast.noForecastFound)
        case AppError.noNetworkConnection,
             AppError.timeout:
            message = localisation.localize(key: LocalisationKey.Common.networkConnectionError)
        default:
            message = localisation.localize(key: LocalisationKey.Common.genericError)
        }
        showMessage(message)
    }
}
