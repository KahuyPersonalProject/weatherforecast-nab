//
//  SearchWeatherForcastPresentable.swift
//  howweather
//
//  Created by Kieu Anh Huy on 22/5/21.
//

import Foundation

protocol SearchWeatherForecastPresentable: Presentable {
    func search(_ text: String)
    func cancelSearch()
}
