//
//  LoadingTableViewCell.swift
//  howweather
//
//  Created by Kieu Anh Huy on 23/5/21.
//

import UIKit

class LoadingTableViewCell: UITableViewCell {
    lazy var loadingIndicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView()
        indicator.translatesAutoresizingMaskIntoConstraints = false
        indicator.hidesWhenStopped = true
        indicator.accessibilityIdentifier = AccessibilityId.SearchWeatherForecast.loadingIndicator
        return indicator
    }()

    override init(style: UITableViewCell.CellStyle,
                  reuseIdentifier: String?) {
        super.init(style: style,
                   reuseIdentifier: reuseIdentifier)
        setupUI()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupUI()
    }

    private func setupUI() {
        self.separatorInset = UIEdgeInsets(top: 0,
                                           left: UIScreen.main.bounds.width,
                                           bottom: 0,
                                           right: 0)
        self.contentView.addSubview(loadingIndicator)
        let constraints = [
            loadingIndicator.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 20),
            loadingIndicator.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -20),
            loadingIndicator.centerXAnchor.constraint(equalTo: self.contentView.centerXAnchor)
        ]
        NSLayoutConstraint.activate(constraints)
    }

    func showLoading() {
        loadingIndicator.startAnimating()
    }
}
