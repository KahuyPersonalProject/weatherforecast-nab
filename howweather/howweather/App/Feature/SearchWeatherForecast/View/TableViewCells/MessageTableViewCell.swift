//
//  EmptyListTableViewCell.swift
//  howweather
//
//  Created by Kieu Anh Huy on 23/5/21.
//

import UIKit

class MessageTableViewCell: UITableViewCell {
    lazy var messageLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.numberOfLines = 0
        label.accessibilityIdentifier = AccessibilityId.SearchWeatherForecast.messageLabel
        return label
    }()

    override init(style: UITableViewCell.CellStyle,
                  reuseIdentifier: String?) {
        super.init(style: style,
                   reuseIdentifier: reuseIdentifier)
        setupUI()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupUI()
    }

    private func setupUI() {
        self.separatorInset = UIEdgeInsets(top: 0,
                                           left: UIScreen.main.bounds.width,
                                           bottom: 0,
                                           right: 0)
        self.contentView.addSubview(messageLabel)
        let constraints = [
            messageLabel.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 20),
            messageLabel.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 20),
            messageLabel.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -20),
            messageLabel.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -20)
        ]
        NSLayoutConstraint.activate(constraints)
    }

    func updateMessage(_ message: String) {
        messageLabel.text = message
    }
}
