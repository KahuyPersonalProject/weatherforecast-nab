//
//  WeatherForecastTableViewCell.swift
//  howweather
//
//  Created by Kieu Anh Huy on 23/5/21.
//

import UIKit

class WeatherForecastTableViewCell: UITableViewCell {
    lazy var dateLabel: UILabel = {
        let label = createLabel()
        label.accessibilityIdentifier = AccessibilityId.SearchWeatherForecast.forecastDateLabel
        return label
    }()

    lazy var temperatureLabel: UILabel = {
        let label = createLabel()
        label.accessibilityIdentifier = AccessibilityId.SearchWeatherForecast.forecastTemperatureLabel
        return label
    }()

    lazy var pressureLabel: UILabel = {
        let label = createLabel()
        label.accessibilityIdentifier = AccessibilityId.SearchWeatherForecast.forecastPressureLabel
        return label
    }()

    lazy var humidityLabel: UILabel = {
        let label = createLabel()
        label.accessibilityIdentifier = AccessibilityId.SearchWeatherForecast.forecastHumidityLabel
        return label
    }()

    lazy var descriptionLabel: UILabel = {
        let label = createLabel()
        label.accessibilityIdentifier = AccessibilityId.SearchWeatherForecast.forecastDescrptionLabel
        return label
    }()

    lazy var contentStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 5
        return stackView
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupUI()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupUI()
    }

    private func setupUI() {
        backgroundColor = UIColor.lightGray.withAlphaComponent(0.5)
        contentStackView.addArrangedSubview(dateLabel)
        contentStackView.addArrangedSubview(temperatureLabel)
        contentStackView.addArrangedSubview(pressureLabel)
        contentStackView.addArrangedSubview(humidityLabel)
        contentStackView.addArrangedSubview(descriptionLabel)
        self.contentView.addSubview(contentStackView)
        let constraints = [
            contentStackView.topAnchor.constraint(equalTo: self.contentView.topAnchor,
                                                  constant: 10),
            contentStackView.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor,
                                                      constant: 15),
            contentStackView.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor,
                                                     constant: -10),
            contentStackView.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor,
                                                       constant: -15)
        ]
        NSLayoutConstraint.activate(constraints)
    }

    private func createLabel() -> UILabel {
        let label = UILabel()
        label.font = .preferredFont(forTextStyle: .body)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        return label
    }

    func updateCell(_ model: WeatherForecastDisplayable) {
        dateLabel.text = model.displayDate
        temperatureLabel.text = model.displayTemperature
        pressureLabel.text = model.displayPressure
        humidityLabel.text = model.displayHumidity
        descriptionLabel.text = model.displayDescription
    }
}
