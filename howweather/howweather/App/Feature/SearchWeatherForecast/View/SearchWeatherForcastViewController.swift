//
//  SearchWeatherForcastViewController.swift
//  howweather
//
//  Created by Kieu Anh Huy on 22/5/21.
//

import UIKit

class SearchWeatherForecastViewController<P: SearchWeatherForecastPresentable>: BaseViewController<P> {
    override func loadView() {
        let view = UIView()
        view.backgroundColor = .red
        self.view = view
    }
}

extension SearchWeatherForecastViewController: SearchWeatherForcastViewable {
    func showWeatherForecastData() {
    }
}
