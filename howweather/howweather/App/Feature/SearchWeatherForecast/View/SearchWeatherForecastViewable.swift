//
//  SearchWeatherForcastViewable.swift
//  howweather
//
//  Created by Kieu Anh Huy on 22/5/21.
//

import Foundation

protocol SearchWeatherForecastViewable: AnyObject {
    func reloadCells(_ cells: [WeatherForecastCellType])
    func showTitle(_ title: String)
}
