//
//  SearchWeatherForecastView.swift
//  howweather
//
//  Created by Kieu Anh Huy on 22/5/21.
//

import UIKit

class SearchWeatherForecastView: UIView {
    lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.tableFooterView = UIView()
        tableView.dataSource = resultListDataSource
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = UITableView.automaticDimension
        tableView.allowsSelection = false
        tableView.keyboardDismissMode = .onDrag
        tableView.accessibilityIdentifier = AccessibilityId.SearchWeatherForecast.tableView
        self.registerCellForTableView(tableView)
        return tableView
    }()

    lazy var searchBar: UISearchBar = {
        let searchBar = UISearchBar()
        searchBar.translatesAutoresizingMaskIntoConstraints = false
        searchBar.showsCancelButton = true
        searchBar.delegate = self
        searchBar.searchBarStyle = .minimal
        searchBar.accessibilityIdentifier = AccessibilityId.SearchWeatherForecast.searchBar
//        searchBar.accessibilityIdentifier = AccessibilityId.SearchWeatherForecast.searchBar
        return searchBar
    }()

    var resultListDataSource = SearchWeatherForecastDataSource()
    weak var delegate: SearchWeatherForecastViewDelegate?

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupUI()
    }

    private func setupUI() {
        backgroundColor = .lightGray
        setupTableView()
        setupSearchBar()
    }

    private func setupTableView() {
        addSubview(tableView)
        let constraints = [
            tableView.topAnchor.constraint(equalTo: topAnchor),
            tableView.leadingAnchor.constraint(equalTo: leadingAnchor),
            tableView.bottomAnchor.constraint(equalTo: bottomAnchor),
            tableView.trailingAnchor.constraint(equalTo: trailingAnchor)
        ]
        NSLayoutConstraint.activate(constraints)
    }

    private func setupSearchBar() {
        let headerView = UIView()
        headerView.translatesAutoresizingMaskIntoConstraints = false
        let searchBarContainerView = UIView()
        searchBarContainerView.translatesAutoresizingMaskIntoConstraints = false
        headerView.addSubview(searchBarContainerView)
        searchBarContainerView.addSubview(searchBar)
        tableView.tableHeaderView = headerView
        let constraints = [
            searchBarContainerView.widthAnchor.constraint(equalTo: tableView.widthAnchor),
            searchBarContainerView.topAnchor.constraint(equalTo: headerView.topAnchor),
            searchBarContainerView.leadingAnchor.constraint(equalTo: headerView.leadingAnchor),
            searchBarContainerView.bottomAnchor.constraint(equalTo: headerView.bottomAnchor),
            searchBarContainerView.trailingAnchor.constraint(equalTo: headerView.trailingAnchor),
            searchBar.topAnchor.constraint(equalTo: searchBarContainerView.topAnchor),
            searchBar.leadingAnchor.constraint(equalTo: searchBarContainerView.leadingAnchor,
                                               constant: 15),
            searchBar.bottomAnchor.constraint(equalTo: searchBarContainerView.bottomAnchor),
            searchBar.trailingAnchor.constraint(equalTo: searchBarContainerView.trailingAnchor,
                                                constant: -15),
            searchBar.heightAnchor.constraint(equalToConstant: 60)
        ]
        NSLayoutConstraint.activate(constraints)
    }

    private func registerCellForTableView(_ tableView: UITableView) {
        tableView.register(LoadingTableViewCell.self,
                           forCellReuseIdentifier: AppConstant.SearchWeatherForecast.loadingCellId)
        tableView.register(MessageTableViewCell.self,
                           forCellReuseIdentifier: AppConstant.SearchWeatherForecast.emptyCellId)
        tableView.register(WeatherForecastTableViewCell.self,
                           forCellReuseIdentifier: AppConstant.SearchWeatherForecast.forecastCellId)
    }

    func reloadData(_ cells: [WeatherForecastCellType]) {
        self.resultListDataSource.updateCells(cells)
        self.tableView.reloadData()
    }
}

// MARK: UISearchBarDelegate
extension SearchWeatherForecastView: UISearchBarDelegate {
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        delegate?.didCancelSearch()
    }

    func searchBar(_ searchBar: UISearchBar,
                   textDidChange searchText: String) {
        delegate?.didChangeSearchText(searchText)
    }
}

protocol SearchWeatherForecastViewDelegate: AnyObject {
    func didCancelSearch()
    func didChangeSearchText(_ text: String)
}
