//
//  SearchWeatherForecastViewController.swift
//  howweather
//
//  Created by Kieu Anh Huy on 22/5/21.
//

import UIKit

class SearchWeatherForecastViewController<P: SearchWeatherForecastPresentable>: BaseViewController<P> {

    lazy var customView: SearchWeatherForecastView = {
        let view = SearchWeatherForecastView()
        view.delegate = self
        return view
    }()

    override func loadView() {
        self.view = customView
    }
}

// MARK: SearchWeatherForecastViewable
extension SearchWeatherForecastViewController: SearchWeatherForecastViewable {
    func reloadCells(_ cells: [WeatherForecastCellType]) {
        customView.reloadData(cells)
    }

    func showTitle(_ title: String) {
        self.title = title
    }
}

// MARK: SearchWeatherForecastViewDelegate
extension SearchWeatherForecastViewController: SearchWeatherForecastViewDelegate {
    func didCancelSearch() {
        presenter.cancelSearch()
    }

    func didChangeSearchText(_ text: String) {
        presenter.search(text)
    }
}
