//
//  SearchWeatherForecastDataSource.swift
//  howweather
//
//  Created by Kieu Anh Huy on 23/5/21.
//

import UIKit

class SearchWeatherForecastDataSource: NSObject, UITableViewDataSource {
    private var cells: [WeatherForecastCellType] = []

    func updateCells(_ cells: [WeatherForecastCellType]) {
        self.cells = cells
    }

    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = cells[indexPath.row]
        switch item {
        case .message(let message):
            let cellId = AppConstant.SearchWeatherForecast.emptyCellId
            let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)
            if let messageCell = cell as? MessageTableViewCell {
                messageCell.updateMessage(message)
            }
            return cell
        case .loading:
            let cellId = AppConstant.SearchWeatherForecast.loadingCellId
            let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)
            if let loadingCell = cell as? LoadingTableViewCell {
                loadingCell.showLoading()
            }
            return cell
        case .forecast(let model):
            let cellId = AppConstant.SearchWeatherForecast.forecastCellId
            let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)
            if let forecastCell = cell as? WeatherForecastTableViewCell {
                forecastCell.updateCell(model)
            }
            return cell
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cells.count
    }
}
