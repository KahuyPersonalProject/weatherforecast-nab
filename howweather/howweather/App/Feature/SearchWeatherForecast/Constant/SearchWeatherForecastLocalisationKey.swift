//
//  SearchWeatherForecastLocalisationKey.swift
//  howweather
//
//  Created by Kieu Anh Huy on 23/5/21.
//

import Foundation

extension LocalisationKey {
    enum SearchWeatherForecast {
        static let title = "searchForecast.forecast.title"
        static let searchInputPlaceHolder = "searchForecast.searchInput.placeHolder"
        static let noForecastFound = "searchForecast.message.noForecastFound"
        static let inputSearch = "searchForecast.message.inputSearch"
        static let forecastDate = "searchForecast.forecastItem.date"
        static let forecastAverageTemperature = "searchForecast.forecastItem.averageTemperature"
        static let forecastPressure = "searchForecast.forecastItem.pressure"
        static let forecastHumidity = "searchForecast.forecastItem.humidity"
        static let forecastDescription = "searchForecast.forecastItem.description"
    }
}
