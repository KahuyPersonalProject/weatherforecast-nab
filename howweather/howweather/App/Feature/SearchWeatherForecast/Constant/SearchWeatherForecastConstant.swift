//
//  SearchWeatherForecastConstant.swift
//  howweather
//
//  Created by Kieu Anh Huy on 22/5/21.
//

import Foundation

extension AppConstant {
    enum SearchWeatherForecast {
        static let defaultUnit = ForecastUnit.celsius
        static let defaultNumberDay = 7
        static let forecastDisplayDateFormat = "EEE, dd MMM yyyy"
        static let numberCharacterStartSearch = 3
        static let loadingCellId = "\(LoadingTableViewCell.self)"
        static let emptyCellId = "\(MessageTableViewCell.self)"
        static let forecastCellId = "\(WeatherForecastTableViewCell.self)"
    }
}

extension AccessibilityId {
    enum SearchWeatherForecast {
        static let searchBar = "accessibilityId.searchWeatherForecast.searchBar"
        static let tableView = "accessibilityId.searchWeatherForecast.tableView"
        static let loadingIndicator = "accessibilityId.searchWeatherForecast.loadingIndicator"
        static let messageLabel = "accessibilityId.searchWeatherForecast.messageLabel"
        static let forecastDateLabel = "accessibilityId.searchWeatherForecast.forecastDateLabel"
        static let forecastHumidityLabel = "accessibilityId.searchWeatherForecast.forecastHumidityLabel"
        static let forecastPressureLabel = "accessibilityId.searchWeatherForecast.forecastPressureLabel"
        static let forecastTemperatureLabel = "accessibilityId.searchWeatherForecast.forecastTemperatureLabel"
        static let forecastDescrptionLabel = "accessibilityId.searchWeatherForecast.forecastDescrptionLabel"
    }
}
