//
//  SearchWeatherForecastInteractpr.swift
//  howweather
//
//  Created by Kieu Anh Huy on 22/5/21.
//

import Foundation

class SearchWeatherForecastInteractor: SearchWeatherForecastInteractable {
    let forecastService: WeatherForecastServicing

    init(forecastService: WeatherForecastServicing) {
        self.forecastService = forecastService
    }

    func searchWeatherForecastForTerm(_ term: String,
                                      completion: @escaping (ResultData) -> Void) {
        let appId = AppConstant.Common.forecastAppId
        let unit = AppConstant.SearchWeatherForecast.defaultUnit
        let numberForecastDays = AppConstant.SearchWeatherForecast.defaultNumberDay
        let requestModel = WeatherForecastRequestModel(cityName: term,
                                                       numberForecastDays: numberForecastDays,
                                                       appId: appId,
                                                       unit: unit)
        self.forecastService
            .searchWeatherForecastForTerm(requestModel,
                                          completion: {[weak self] result in
                                            guard let self = self else { return }
                                            let result = self.handleSearchForecastResult(result)
                                            completion(result)
                                          })
    }

    private func handleSearchForecastResult(_ result: Result<WeatherForecastResponseModel, Error>) -> ResultData {
        result.map { responseModel -> [WeatherForecastModel] in
            return responseModel.forecastList.map { item -> WeatherForecastModel in
                return WeatherForecastModel(responseModel: item)
            }
        }
    }
}
