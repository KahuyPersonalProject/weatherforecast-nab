//
//  SearchWeatherForecastInteractable.swift
//  howweather
//
//  Created by Kieu Anh Huy on 22/5/21.
//

import Foundation

protocol SearchWeatherForecastInteractable {
    typealias ResultData = Result<[WeatherForecastModel], Error>
    func searchWeatherForecastForTerm(_ term: String,
                                      completion: @escaping (ResultData) -> Void)
}
