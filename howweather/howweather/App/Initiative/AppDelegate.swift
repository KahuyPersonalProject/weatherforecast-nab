//
//  AppDelegate.swift
//  howweather
//
//  Created by Kieu Anh Huy on 22/5/21.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    var rootCoordinator: AppRootCoordinator?

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        let appWindow = UIWindow(frame: UIScreen.main.bounds)
        let rootDependencyContainer = AppDependency()
        rootCoordinator = AppRootCoordinator(appWindow: appWindow,
                                             rootDependencyContainer: rootDependencyContainer)
        rootCoordinator?.start()
        return true
    }
}
