//
//  AppConstant.swift
//  howweather
//
//  Created by Kieu Anh Huy on 22/5/21.
//

import Foundation

enum AppConstant {
    enum Common {
        static let forecastAppId = "60c6fbeb4b93ac653c492ba806fc346d"
        static let baseForecastUrl = "https://api.openweathermap.org"
    }
}

enum AccessibilityId {
}
