//
//  LocalisationKey.swift
//  howweather
//
//  Created by Kieu Anh Huy on 23/5/21.
//

import Foundation

enum LocalisationKey {
    enum Common {
        static let networkConnectionError = "common.message.networkConnectionError"
        static let genericError = "common.message.genericError"
    }
}
