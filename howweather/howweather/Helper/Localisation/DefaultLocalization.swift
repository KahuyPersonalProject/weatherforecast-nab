//
//  DefaultLocalization.swift
//  howweather
//
//  Created by Kieu Anh Huy on 23/5/21.
//

import Foundation

public class DefaultLocalization: Localizable {

    private var bundle: Bundle

    public init(bundle: Bundle) {
        self.bundle = bundle
    }

    public func localize(key: String) -> String {
        return bundle.localizedString(forKey: key, value: key, table: nil)
    }
}
