//
//  Localizable.swift
//  howweather
//
//  Created by Kieu Anh Huy on 23/5/21.
//

import Foundation

public protocol Localizable {
    func localize(key: String) -> String
}
