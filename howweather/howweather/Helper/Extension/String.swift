//
//  String.swift
//  howweather
//
//  Created by Kieu Anh Huy on 22/5/21.
//

import Foundation

extension String {
    func encodeUTF8() -> String? {
        if URL(string: self) != nil {
            return self
        }

        var components = self.components(separatedBy: "/")
        guard let lastComponent = components.popLast(),
              let endcodedLastComponent = lastComponent.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        else {
            return nil
        }

        return (components + [endcodedLastComponent]).joined(separator: "/")
    }
}
