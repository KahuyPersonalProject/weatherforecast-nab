//
//  SimpleDependencyContainer.swift
//  howweather
//
//  Created by Kieu Anh Huy on 22/5/21.
//

import Foundation

public class SimpleDependencyContainer: DependencyContainer {

    var serviceContainer = [DepedencyServiceKey: DepdencyFactory]()

    private weak var parentContainer: DependencyContainer?

    public init(parentContainer: DependencyContainer? = nil) {
        self.parentContainer = parentContainer
    }

    public func register(components: [ComponentDependency]) {
        components.forEach { component in
            component.register(container: self)

        }
    }

    public func register<Service>(_ type: Service.Type,
                                  factory: @escaping ((DependencyContainer) -> Service),
                                  scope: Scope) {
        register(type, name: nil, factory: factory, scope: scope)
    }

    public func register<Service>(_ type: Service.Type,
                                  name: String?,
                                  factory: @escaping ((DependencyContainer) -> Service),
                                  scope: Scope) {
        let serviceName = name ?? String(describing: type)
        let serviceKey = DepedencyServiceKey(name: serviceName, type: type)
        let factory = DepdencyFactory(scope: scope, factory: factory, instance: nil)
        serviceContainer[serviceKey] = factory
    }

    public func resolve<Service>(_ type: Service.Type,
                                 name: String?) -> Service? {
        let instance = resolveInCurrencyContainer(type: type, name: name)
        return instance ?? parentContainer?.resolve(type, name: name)
    }

    public func resolve<Service>(_ type: Service.Type) -> Service? {
        return resolve(type, name: nil)
    }

    private func resolveInCurrencyContainer<Service>(type: Service.Type,
                                                     name: String?) -> Service? {
        guard var factory = getFactory(type: type, name: name) else { return nil }
        var objectInstance: Service?
        switch factory.scope {
        case .container:
            if let instance = factory.instance as? Service {
                objectInstance = instance
            } else {
                guard let instance = factory.factory(self) as? Service else { return nil}
                factory.instance = instance
                addFactory(factory, type: type, name: name)
                objectInstance = instance
            }
        case .transient:
            objectInstance = factory.factory(self) as? Service
        }

        return objectInstance
    }

    private func addFactory<Service>(_ factory: DepdencyFactory,
                                     type: Service.Type,
                                     name: String?) {
        let serviceName = name ?? String(describing: type)
        let serviceKey = DepedencyServiceKey(name: serviceName, type: type)
        serviceContainer[serviceKey] = factory
    }

    private func getFactory<Service>(type: Service.Type,
                                     name: String?) -> DepdencyFactory? {
        let serviceName = name ?? String(describing: type)
        let serviceKey = DepedencyServiceKey(name: serviceName, type: type)

        return serviceContainer[serviceKey]
    }

    deinit {
        serviceContainer.removeAll()
    }
}
