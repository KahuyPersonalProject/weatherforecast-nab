//
//  DependencyContainer.swift
//  howweather
//
//  Created by Kieu Anh Huy on 22/5/21.
//

import Foundation

/// Instance's scope
/// container: Instance will be share in container. Singletone in container
/// transient: Create new instance every request
public enum Scope {
    case container
    case transient
}

public protocol DependencyContainer: AnyObject {

    /// Register component dependencies with container
    func register(components: [ComponentDependency])

    /// Register implementation for type and name.
    /// factory will be called once user request new instance
    func register<Service>(_ type: Service.Type,
                           name: String?,
                           factory: @escaping ((DependencyContainer) -> Service),
                           scope: Scope)

    /// Register implementation for type and name.
    /// factory will be called once user request new instance
    func register<Service>(_ type: Service.Type,
                           factory: @escaping ((DependencyContainer) -> Service),
                           scope: Scope)

    /// Return instance for Type and name
    func resolve<Service>(_ type: Service.Type,
                          name: String?) -> Service?

    /// Return instance for Type and name
    func resolve<Service>(_ type: Service.Type) -> Service?
}
