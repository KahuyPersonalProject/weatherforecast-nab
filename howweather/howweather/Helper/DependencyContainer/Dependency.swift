//
//  Dependency.swift
//  howweather
//
//  Created by Kieu Anh Huy on 22/5/21.
//

import Foundation

public protocol Dependency {
    func resolve<S>(_ type: S.Type) -> S?
    func resolve<S>(_ type: S.Type, name: String) -> S?
}

open class SimpleDependency: Dependency {

    public var container: DependencyContainer

    public init(parent: SimpleDependency? = nil) {
        container = SimpleDependencyContainer(parentContainer: parent?.container)
        setup()
    }

    open func setup() {}

    open func resolve<S>(_ type: S.Type) -> S? {
        return container.resolve(type)
    }

    open func resolve<S>(_ type: S.Type, name: String) -> S? {
        return container.resolve(type, name: name)
    }
}
