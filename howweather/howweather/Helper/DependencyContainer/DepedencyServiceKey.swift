//
//  DepedencyServiceKey.swift
//  howweather
//
//  Created by Kieu Anh Huy on 22/5/21.
//

import Foundation

struct DepedencyServiceKey {
    var name: String
    var type: Any.Type
}

// MARK: Equatable
extension DepedencyServiceKey: Equatable {
    static func == (lhs: DepedencyServiceKey, rhs: DepedencyServiceKey) -> Bool {
        return lhs.name == rhs.name
            && lhs.type == rhs.type
    }
}

// MARK: Hashable
extension DepedencyServiceKey: Hashable {
    func hash(into hasher: inout Hasher) {
        ObjectIdentifier(type).hash(into: &hasher)
        name.hash(into: &hasher)
    }
}
