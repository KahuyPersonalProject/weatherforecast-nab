//
//  ComponentDependency.swift
//  howweather
//
//  Created by Kieu Anh Huy on 22/5/21.
//

import Foundation

public protocol ComponentDependency {
    func register(container: DependencyContainer)
}
