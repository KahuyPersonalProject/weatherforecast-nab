//
//  DepdencyFactory.swift
//  howweather
//
//  Created by Kieu Anh Huy on 22/5/21.
//

import Foundation

struct DepdencyFactory {
    var scope: Scope
    var factory: ((DependencyContainer) -> Any)
    var instance: Any?
}
