//
//  DispatchSync.swift
//  howweather
//
//  Created by Kieu Anh Huy on 23/5/21.
//

import Foundation

class DispatchSync: Dispatcher {}

extension DispatchSync: Dispatching {
    func dispatch(_ work: @escaping () -> Void) {
        queue.sync(execute: work)
    }

    func dispatchAfter(deadline: DispatchTime, workItem: DispatchWorkItem) {
        queue.sync(execute: workItem)
    }
}

extension DispatchSync {
    static let main: DispatchSync = DispatchSync(queue: .main)
    static let global: DispatchSync = DispatchSync(queue: .global())
    static let background: DispatchSync = DispatchSync(queue: .global(qos: .background))
}
