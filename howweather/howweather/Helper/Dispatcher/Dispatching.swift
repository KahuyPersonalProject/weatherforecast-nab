//
//  Dispatching.swift
//  howweather
//
//  Created by Kieu Anh Huy on 23/5/21.
//

import Foundation

public protocol Dispatching {
    func dispatch(_ work: @escaping () -> Void)

    func dispatchAfter(deadline: DispatchTime, workItem: DispatchWorkItem)
}
