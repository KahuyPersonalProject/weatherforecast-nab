//
//  DispatchQueues.swift
//  howweather
//
//  Created by Kieu Anh Huy on 23/5/21.
//

import Foundation

public protocol DispatchQueues {
    var main: Dispatching { get }
    var background: Dispatching { get }
}

public class DispatchQueueImpl: DispatchQueues {
    public var main: Dispatching
    public var background: Dispatching

    public init(mainDispatch: Dispatching, backgroundDispatch: Dispatching) {
        self.main = mainDispatch
        self.background = backgroundDispatch
    }
}
