//
//  DispatchAsync.swift
//  howweather
//
//  Created by Kieu Anh Huy on 23/5/21.
//

import Foundation

public class DispatchAsync: Dispatcher {}

extension DispatchAsync: Dispatching {
    public func dispatch(_ work: @escaping () -> Void) {
        queue.async(execute: work)
    }

    public func dispatchAfter(deadline: DispatchTime, workItem: DispatchWorkItem) {
        queue.asyncAfter(deadline: deadline, execute: workItem)
    }
}

extension DispatchAsync {
    public static let main: DispatchAsync = DispatchAsync(queue: .main)
    public static let global: DispatchAsync = DispatchAsync(queue: .global())
    public static let background: DispatchAsync = DispatchAsync(queue: .global(qos: .background))
}
