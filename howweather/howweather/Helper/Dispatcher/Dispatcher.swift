//
//  Dispatcher.swift
//  howweather
//
//  Created by Kieu Anh Huy on 23/5/21.
//

import Foundation

public class Dispatcher {
    let queue: DispatchQueue

    init(queue: DispatchQueue) {
        self.queue = queue
    }
}
