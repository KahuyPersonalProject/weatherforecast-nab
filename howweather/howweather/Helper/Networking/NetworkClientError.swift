//
//  NetworkClientError.swift
//  howweather
//
//  Created by Kieu Anh Huy on 22/5/21.
//

import Foundation

enum NetworkClientError: Error {
    case invalidUrl(String)
    case httpError(Int, String, Data?)
    case unknown(URLResponse?)
    case timeout
    case noNetworkConnection
}
