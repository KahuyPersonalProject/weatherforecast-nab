//
//  NetworkClientResponse.swift
//  howweather
//
//  Created by Kieu Anh Huy on 22/5/21.
//

import Foundation

struct NetworkClientResponse {
    let urlRequest: URLRequest
    let responseData: Data?
    let response: URLResponse?
    let error: Error?
}
