//
//  ParameterEncoding.swift
//  howweather
//
//  Created by Kieu Anh Huy on 22/5/21.
//

import Foundation

protocol ParameterEncoding {
    func encode(parameters: [String: Any]?, with request: URLRequest) -> URLRequest
}

enum ParameterEncoder {
    case queryString
}

extension ParameterEncoder: ParameterEncoding {
    func encode(parameters: [String: Any]?, with request: URLRequest) -> URLRequest {
        switch self {
        case .queryString:
            return applyQuery(parameters: parameters, to: request)
        }
    }

    private func applyQuery(parameters: [String: Any]?,
                            to request: URLRequest) -> URLRequest {
        guard let parameters = parameters,
              !parameters.isEmpty
        else { return request }
        let queryItems = parameters.map { (key, value) -> URLQueryItem in
            return URLQueryItem(name: key, value: "\(value)")
        }
        guard let url = request.url else { return request }
        var urlComponent = URLComponents(url: url, resolvingAgainstBaseURL: false)
        urlComponent?.queryItems = queryItems
        var editedRequest = request
        editedRequest.url = urlComponent?.url
        return editedRequest
    }
}
