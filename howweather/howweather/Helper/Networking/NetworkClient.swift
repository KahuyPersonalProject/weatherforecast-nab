//
//  NetworkClient.swift
//  howweather
//
//  Created by Kieu Anh Huy on 22/5/21.
//

import Foundation

class NetworkClient {
    let baseUrl: String
    let session: URLSession

    init(baseUrl: String,
         session: URLSession) {
        self.baseUrl = baseUrl
        self.session = session
    }

    func request(path: String,
                 method: RequestMethod,
                 parameterEncoder: ParameterEncoding,
                 requestParameter: [String: Any]?,
                 headers: [String: String]? = nil,
                 completion: @escaping (Result<Data, Error>) -> Void) {
        do {
            let url = try composeUrl(with: path)
            var urlRequest = URLRequest(url: url)
            urlRequest.httpMethod = method.rawValue
            urlRequest = parameterEncoder.encode(parameters: requestParameter,
                                                 with: urlRequest)
            requestData(urlRequest: urlRequest, completion: completion)
        } catch {
            completion(.failure(error))
            return
        }
    }

    private func composeUrl(with path: String) throws -> URL {
        let encodedPath = baseUrl + (path.encodeUTF8() ?? path)
        guard let url = URL(string: encodedPath)
        else {
            let message = "Couldn't create a url using baseURL: \(baseUrl) and encodedPath: \(encodedPath)"
            throw NetworkClientError.invalidUrl(message)
        }
        return url
    }

    func requestData(urlRequest: URLRequest,
                     completion:  @escaping (Result<Data, Error>) -> Void) {
        let task = session.dataTask(with: urlRequest) {[weak self] (responseData, response, error) in
            guard let self = self else { return }
            let networkResponse = NetworkClientResponse(urlRequest: urlRequest,
                                                        responseData: responseData,
                                                        response: response,
                                                        error: error)
            let result: Result<Data, Error> = self.handleRequestResponse(networkResponse: networkResponse)
            completion(result)
        }
        let requestID = UUID().uuidString
        task.taskDescription = requestID
        task.resume()
    }

    private func handleRequestResponse(networkResponse: NetworkClientResponse) -> Result<Data, Error> {
        var result: Result<Data, Error>
        if let httpResponse = networkResponse.response as? HTTPURLResponse {
            if 200...299 ~= httpResponse.statusCode {
                if let data = networkResponse.responseData,
                   data.count > 0 {
                    result = .success(data)
                } else {
                    result = Result.failure(NetworkClientError.unknown(networkResponse.response))
                }
            } else {
                let errorCode = httpResponse.statusCode
                let message = HTTPURLResponse.localizedString(forStatusCode: httpResponse.statusCode)
                let error = NetworkClientError.httpError(errorCode, message, networkResponse.responseData)
                result = Result.failure(error)
            }
        } else {
            let statusCode = (networkResponse.error as NSError?)?.code ?? 200
            var networkError: Error
            switch statusCode {
            case URLError.timedOut.rawValue:
                networkError = NetworkClientError.timeout
            case URLError.notConnectedToInternet.rawValue:
                networkError = NetworkClientError.noNetworkConnection
            default:
                let url =  networkResponse.urlRequest.url
                let response = HTTPURLResponse(url: url!, statusCode: statusCode, httpVersion: nil, headerFields: nil)!
                networkError = NetworkClientError.unknown(response)
            }
            result = Result.failure(networkError)
        }

        return result
    }
}
