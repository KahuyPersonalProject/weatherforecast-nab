# Weather Forecast
## Pattern Explanation
### Main Pattern
I using VIPEC
**V**: View is what user sees. it represents a functionality
**I**: Interactor is response for logic handling and transform data from outside to component data.
**P**: Presenter is response for connecting components. It formats display data, receives user action from view, sends and receive data from interactor, and trigger navigation throught coordinator
**E**: Entity is data model which is used on the functionality
**C**: Coordinator control navigation the screen or flow. It will organize as a tree coordinator. Evey screen is going to have a coordinator and every follow is go to have a root coordinator
RootCoordinator
+-- Feature 1 Coordinator
|   +-- Screen 1.1 Coordinator
|   +-- Screen 1.2 Coordinator
+-- Feature 2 Coordinator
|   +-- Screen 2.1 Coordinator
|   +-- Screen 2.2 Coordinator
+-- ...
### Dependecy flow
Upper layer might depend on lower layer but Lower layer should never depend on upper layer

### Dependency Injection
I implement a simple dependency container to support dependency injection for the application. Developer can Register and Resolve dependencies through protocol without knowledge about concrete classes

### Page Object patter
The pattern is applied for acceptance test which hide complexity of accessing, verifying ui element in a class. It help testcases to do anything without hassle.

## Code Structure
The application is a mix match between package by feature (every feature will self-contained it classes) and package by layer (Common service will be packaged together).
howweather
+-- App - Contain app's classes
|   +-- Initiative - Contain app entry point
|   +-- Feature - Contain all feature of the application
|   |   +-- SearchWeatherForecast - Contain View-Presenter-Interactor-Entiy of the feature only
|   |   +-- AppRoot - Contain feature entry point
|   +-- Service
|   |   +-- WeatherForeCast - Contain common forecast service for all feature
|   +-- NetworkClient - Contain network client which can contain specific logic for the app only
|   +-- Constant
|   +-- Resources
|   |   +-- AcceptanceTest - Contain Stub classes to break to dependency from real service
+-- Helper
|   +-- Networking - as a simple networking library which provide network client and can reuse in multiple app
|   +-- DependencyContainer - Implementation of simple dependency container
|   +-- Localisation
|   +-- Dispatcher - is a thead dispatcher which help presenter easier to unit test without GCD hassle
|   +-- Extension
+-- .

## Run Step
Source code is free library and framework. reviewer can run directly from Xcode and Simulator which support iOS 13 and later
For Unit test:
  1. Select howweather Scheme
  2. Run Unit Test
For Acceptance test: Because the application depends on external system, we're unable exeptional and expectation data. Therefore, we need to create stub data at url session.
  1. Select HowWeather-AcceptanceTest Scheme
  2. Run Test

## Check List
- [x] Programming language: Swift is required, Objective-C is optional.
- [x] Design app's architecture (recommend VIPER or MVP, MVVM but not mandatory)
- [x] UI should be looks like in attachment.
- [x] Write UnitTests
- [x] Acceptance Tests
- [x] Exception handling
- [x] Caching handling
- [x] Accessibility for Disability Supports:
  - [x] VoiceOver: Use a screen reader.
  - [x] Scaling Text: Display size and font size: To change the size of items on your screen, adjust the display size or font size.
- [x] Entity relationship diagram for the database and solution diagrams for the components, infrastructure design if any
- [x] ReadMe
